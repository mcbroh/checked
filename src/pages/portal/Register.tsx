import React from 'react';
import { IonGrid, IonRow, IonCol, IonContent, IonButton, IonLabel, IonPage, } from '@ionic/react';

import { Input, AppButton, MessageBoard, Header, Logo } from '../../components';
import { backendService, languageService } from '../../service';

class Register extends React.Component<any, any> {

    constructor(props: any) {
        super(props);

        const userPreset = JSON.parse(localStorage.getItem('USER_PRESET') as any);

        this.state = {
            formInvalid: {
                name: true,
                email: true,
                phone: true,
                street: true,
                streetNo: true,
                companyId: true,
                companyName: true,
            },
            showRegistered: false,
            formValues: {
                name: '',
                email: '',
                city: userPreset.city,
                phone: '',
                street: '',
                streetNo: '',
                companyId: '',
                companyName: '',
                country: userPreset.country,
                organization: '',
                organizationPhone: '',
            }
        };
    };

    inputChange(value: any, validity: boolean, field: any) {
        this.setState({
            ...this.state,
            formValues: {
                ...this.state.formValues,
                [field]: value,
            },
            formInvalid: {
                ...this.state.formInvalid,
                [field]: validity,
            },
        });
    };

    submitForm() {
        this.props._appLoading(true);
        backendService.post('companies/application', this.state.formValues).then((response: any) => {
            if (response.success) {
                this.setState({ showRegistered: true })
            };
            this.props._appLoading(false, {
                color: response.success ? 'success' : 'danger',
                message: response.message,
                showToast: true
            });
        })
    }

    goToSignIn(): void {
        this.setState({ showRegistered: false }, function () {
            window.location.href = '/login';
        })
    }

    private renderForm(): React.ReactNode {
        const { formValues: { name, email, city, phone, street, streetNo, companyId, companyName, country, organization, organizationPhone }, formInvalid } = this.state;
        const isValid = Object.values(formInvalid).includes(true);
        return (
            <IonGrid>
                <IonRow>
                    <IonCol size='12' sizeSm='8' pushSm='2' sizeXl='6' pushXl='3'>
                        <section className='card'>
                            <Logo />
                            <h1>{languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'register')}</h1>
                            <IonGrid>
                                <IonRow>
                                    <IonCol size='12'>
                                        <Input
                                            value={name}
                                            rule='required'
                                            label={languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'name')}
                                            onChange={(value: any, validity: boolean) => this.inputChange(value, validity, 'name')} />
                                    </IonCol>
                                    <IonCol size='12'>
                                        <Input
                                            value={companyName}
                                            rule='required'
                                            label={languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'companyName')}
                                            onChange={(value: any, validity: boolean) => this.inputChange(value, validity, 'companyName')} />
                                    </IonCol>
                                    <IonCol size='12'>
                                        <Input
                                            value={email}
                                            type='email' rule='email'
                                            label={languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'email')}
                                            onChange={(value: any, validity: boolean) => this.inputChange(value, validity, 'email')} />
                                    </IonCol>
                                    <IonCol size='12'>
                                        <Input
                                            value={phone}
                                            rule='required'
                                            label={languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'phone')}
                                            onChange={(value: any, validity: boolean) => this.inputChange(value, validity, 'phone')} />
                                    </IonCol>
                                    <IonCol size='12'>
                                        <Input
                                            value={street}
                                            rule='required'
                                            label={languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'street')}
                                            onChange={(value: any, validity: boolean) => this.inputChange(value, validity, 'street')} />
                                    </IonCol>
                                    <IonCol size='12'>
                                        <Input
                                            value={streetNo}
                                            rule='required'
                                            label={languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'streetNo')}
                                            onChange={(value: any, validity: boolean) => this.inputChange(value, validity, 'streetNo')} />
                                    </IonCol>

                                    <IonCol size='12'>
                                        <IonLabel><b>{languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'organization')}</b></IonLabel>
                                    </IonCol>
                                    <IonCol size='12'>
                                        <Input
                                            value={organization}
                                            label={languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'organization')}
                                            onChange={(value: any, validity: boolean) => this.inputChange(value, validity, 'organization')} />
                                    </IonCol>
                                    <IonCol size='12'>
                                        <Input
                                            value={companyId}
                                            rule='required'
                                            label={languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'companyId')}
                                            onChange={(value: any, validity: boolean) => this.inputChange(value, validity, 'companyId')} />
                                    </IonCol>
                                    <IonCol size='12'>
                                        <Input
                                            value={organizationPhone}
                                            label={languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'phone')}
                                            onChange={(value: any, validity: boolean) => this.inputChange(value, validity, 'organizationPhone')} />
                                    </IonCol>

                                    <IonCol size='12'>
                                        <IonLabel><b>{languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'location')}</b></IonLabel>
                                    </IonCol>
                                    <IonCol size='12'>
                                        <Input
                                            disabled
                                            value={city}
                                            onChange={() => { }}
                                            label={languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'city')} />
                                    </IonCol>
                                    <IonCol size='12'>
                                        <Input
                                            disabled
                                            value={getCountryName(country)}
                                            onChange={() => { }}
                                            label={languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'country')} />
                                    </IonCol>

                                    <AppButton text={languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'submit')} disabled={isValid} onClick={() => this.submitForm()} />
                                    <div className='anchor-button-container'>
                                        <IonButton
                                            fill='clear'
                                            className='anchor-btn'
                                            onClick={() => {
                                                this.props.history.push('/login')
                                            }}>{languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'login')}</IonButton>
                                    </div>
                                </IonRow>
                            </IonGrid>
                        </section>
                    </IonCol>
                </IonRow>
            </IonGrid>
        );
    }

    render() {
        return (
            <IonPage>
                <Header />
                <IonContent className='portal'>
                    {!this.state.showRegistered ? this.renderForm() : <MessageBoard message={languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'registration_done')} close={() => this.goToSignIn()} />}
                </IonContent>
            </IonPage>
        );
    }
};

export default Register;

function getCountryName(argCountry: string) {
    const countriesList = JSON.parse(localStorage.getItem('APP_CONFIG') as string)['countriesList'] as Array<any>;
    const selectedCountry = countriesList.filter(country => country.code === argCountry)[0];
    return selectedCountry.name;
}
