import React, { useState } from 'react'
import { IonContent, IonGrid, IonRow, IonCol, IonButton, IonPage } from '@ionic/react';

import { Input, AppButton, Logo, Header } from '../../components';
import { languageService, backendService } from '../../service';

type State = {
    password: string;
    showPassword: boolean;
};

const ResetPassword: React.FC<any> = (props) => {
    const { history, match: { params: { userId, token } } } = props;
    const [state, setState] = useState<State>({
        password: '',
        showPassword: false,
    });
    const isValid = state.password === '';

    function sendPasswordResetEmail() {
        const dataToMutate = {
            token,
            userId,
            type: 'company',
            password: state.password,
        };

        props._appLoading(true);
        backendService.post('password/new', dataToMutate)
            .then(response => {
                if (response.success) {
                    setTimeout(() => {
                        window.location.href = '/login';
                    }, 1000);
                    setState({
                        ...state,
                        password: '',
                    })
                };
                props._appLoading(false, {
                    color: response.success ? 'success' : 'danger',
                    message: response.message || languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'success'),
                    showToast: true
                });
            })
    }

    return (
        <IonPage>
            <Header />
            <IonContent className='portal'>
                <IonGrid>
                    <IonRow>
                        <IonCol size='12' sizeSm='8' pushSm='2' sizeXl='6' pushXl='3'>
                            <section className='card'>
                                <Logo />
                                <h1>{languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'password_reset')}</h1>
                                <IonGrid>
                                    <IonRow>
                                        <IonCol size='12'>
                                            <Input
                                                value={state.password}
                                                label={languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'password')}
                                                type={state.showPassword ? 'text' : 'password'}
                                                rule='password'
                                                onChange={(value: any, validity: boolean) => setState({ ...state, password: value })}
                                            />
                                            <div className='anchor-button-container'>
                                                <IonButton
                                                    fill='clear'
                                                    className='anchor-btn'
                                                    onClick={() => setState({ ...state, showPassword: !state.showPassword })}>
                                                    {languageService.getText(localStorage.getItem('USER_LANGUAGE'), (state.showPassword ? 'hide_password' : 'show_password'))}
                                                </IonButton>
                                            </div>
                                        </IonCol>
                                        <AppButton text={languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'submit')} disabled={isValid} onClick={() => sendPasswordResetEmail()} />

                                        <div className='anchor-button-container'>
                                            <IonButton
                                                fill='clear'
                                                className='anchor-btn'
                                                onClick={() => {
                                                    history.push('/login')
                                                }}>{languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'login')}</IonButton>
                                        </div>
                                    </IonRow>
                                </IonGrid>

                            </section>
                        </IonCol>
                    </IonRow>
                </IonGrid>
            </IonContent>
        </IonPage>
    );
};

export default ResetPassword
