import React from 'react';
import { IonGrid, IonRow, IonCol, IonContent, IonButton, IonPage } from '@ionic/react';

import { Input, AppButton, Logo, Header } from '../../components';
import { authenticationService, languageService } from '../../service';

class Login extends React.Component<any, any> {

    state = {
        username: '',
        password: '',
        formInvalid: {
            username: true,
            password: true,
        },
    }

    inputChange(value: any, validity: boolean, field: any) {
        this.setState({
            [field]: value,
            formInvalid: {
                ...this.state.formInvalid,
                [field]: validity,
            },
        });
    }

    submitForm() {
        const { username, password } = this.state
        this.setState({ checkValidity: true }, () => {
            if (username && password) {
                this.props._appLoading(true);
                authenticationService.login({ username, password }).then((response: any) => {
                    if (response.success) {
                        localStorage.setItem('TOKEN', response.token);
                        localStorage.setItem('USER', JSON.stringify(response.user));
                        const event = new CustomEvent('loggedIn', {
                            detail: true,
                        });
                        window.dispatchEvent(event);
                    }
                    this.props._appLoading(false, {
                        color: response.success ? 'success' : 'danger',
                        message: response.message,
                        showToast: true
                    });
                })
            }
        })
    }

    render() {
        const { username, password, formInvalid } = this.state;
        const isValid = Object.values(formInvalid).includes(true)
        return (
            <IonPage>
                <Header />
                <IonContent className='portal'>
                    <IonGrid>
                        <IonRow>
                            <IonCol size='12' sizeSm='8' pushSm='2' sizeXl='6' pushXl='3'>
                                <section>
                                    <Logo />
                                    <h1>{languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'login')}</h1>
                                    <IonGrid>
                                        <IonRow>
                                            <IonCol size='12'>
                                                <Input
                                                    value={username}
                                                    label={languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'username')}
                                                    rule='required'
                                                    onChange={(value: any, validity: boolean) => this.inputChange(value, validity, 'username')}
                                                />
                                            </IonCol>
                                            <IonCol size='12'>
                                                <Input
                                                    value={password}
                                                    label={languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'password')}
                                                    type='password'
                                                    rule='password'
                                                    onChange={(value: any, validity: boolean) => this.inputChange(value, validity, 'password')}
                                                />
                                            </IonCol>
                                            <AppButton text={languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'submit')} disabled={isValid} onClick={() => this.submitForm()} />
                                            <div className='anchor-button-container space-between'>
                                                <IonButton
                                                    fill='clear'
                                                    className='anchor-btn'
                                                    onClick={() => {
                                                        this.props.history.push('/password/recover')
                                                    }}>{languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'forgot_password')}</IonButton>
                                                <IonButton
                                                    fill='clear'
                                                    className='anchor-btn'
                                                    onClick={() => {
                                                        this.props.history.push('/register')
                                                    }} >{languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'go_to_register')}</IonButton>
                                            </div>
                                        </IonRow>
                                    </IonGrid>
                                </section>
                            </IonCol>
                        </IonRow>
                    </IonGrid>
                </IonContent>
            </IonPage>
        );
    }

};

export default Login;
