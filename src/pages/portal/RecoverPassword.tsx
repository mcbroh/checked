import React, { useState } from 'react'
import { IonContent, IonGrid, IonRow, IonCol, IonButton, IonPage } from '@ionic/react';

import { Input, AppButton, Logo, Header } from '../../components';
import { languageService, backendService } from '../../service';

type State = {
    email: string;
};

const RecoverPassword: React.FC = ({ _appLoading, history }: any) => {

    const [state, setState] = useState<State>({ email: '' });
    const isValid = state.email === '';

    function sendPasswordResetEmail() {
        _appLoading(true)
        backendService.post('password/reset', { email: state.email, type: 'company' })
            .then(response => {
                if (response.success) {
                    setState({
                        ...state,
                        email: '',
                    })
                }
                _appLoading(false, {
                    showToast: true,
                    color: response.success ? 'success' : 'danger',
                    message: response.message || languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'password_reset_success'),
                });
            })
    }

    return (
        <IonPage>
            <Header />
            <IonContent className='portal'>
                <IonGrid>
                    <IonRow>
                        <IonCol size='12' sizeSm='8' pushSm='2' sizeXl='6' pushXl='3'>
                            <section>
                                <Logo />
                                <h1>{languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'password_reset')}</h1>
                                <IonGrid>
                                    <IonRow>
                                        <IonCol size='12'>
                                            <Input
                                                value={state.email}
                                                label={languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'email')}
                                                type='email'
                                                rule='email'
                                                onChange={(value: any, validity: boolean) => setState({ ...state, email: value })}
                                            />
                                        </IonCol>
                                        <AppButton text={languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'submit')} disabled={isValid} onClick={() => sendPasswordResetEmail()} />
                                        <div className='anchor-button-container'>
                                            <IonButton
                                                fill='clear'
                                                className='anchor-btn'
                                                onClick={() => {
                                                    history.push('/login')
                                                }}>{languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'login')}</IonButton>
                                        </div>
                                    </IonRow>
                                </IonGrid>
                            </section>
                        </IonCol>
                    </IonRow>
                </IonGrid>
            </IonContent>
        </IonPage>
    );
};

export default RecoverPassword
