import React, { useRef, useState } from 'react';
import {
    IonCol, IonContent, IonFab, IonFabButton, IonGrid,
    IonInput, IonPage, IonRow, IonItem, IonLabel, IonSelect, IonSelectOption
} from '@ionic/react';
import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles';
import AddAPhoto from '@material-ui/icons/AddAPhoto';
import HighlightOff from '@material-ui/icons/HighlightOff';
import FavoriteRounded from '@material-ui/icons/FavoriteRounded';
import FavoriteBorderRounded from '@material-ui/icons/FavoriteBorderRounded';
import JoditEditor from "jodit-react";

import { AppButton, Header, Input, Select } from '../../components';
import { backendService, languageService } from '../../service';
import { getCountryRegions } from '../../helpers/getCountryRegions';


interface Property {
    pending: boolean;
    showToast: boolean;
    toastMsg: string | undefined;
    form: {
        rent: number | null;
        numOfRooms: number | null;
        totalSpace: number | null;
        location: string;
        introText: string;
        images: any[];
        city: string;
        generalDetails: string;
        advertType: 'rental' | 'sales';
        paymentOptions: '1' | '6' | '12' | 'full'
    },
    formInvalid: {
        rent: boolean;
        numOfRooms: boolean;
        totalSpace: boolean;
        location: boolean;
        introText: boolean;
    }
};

const userCountry = (localStorage.getItem('USER') && JSON.parse(localStorage.getItem('USER') as any).company) || {};

const InitialState: Property = {
    pending: false,
    showToast: false,
    toastMsg: undefined,
    form: {
        rent: null,
        numOfRooms: null,
        totalSpace: null,
        location: '',
        introText: '',
        images: [],
        generalDetails: languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'generalDetailsHelpText'),
        advertType: 'rental',
        paymentOptions: '1',
        city: userCountry.city,
    },
    formInvalid: {
        rent: true,
        numOfRooms: true,
        totalSpace: true,
        location: true,
        introText: true,
    }
}

const outerTheme = createMuiTheme({
    palette: {
        primary: {
            main: '#fafafa',
        },
    },
});

export const AddListing: React.FC = (props: any) => {

    const [property, SetProperty] = useState<Property>(InitialState);
    const inputRef = useRef<HTMLInputElement>(null);
    const { form: { images } } = property;
    const isValid = Object.values(property.formInvalid).includes(true);

    function upload(selectorFiles: FileList | null) {
        const { form } = property;
        if (selectorFiles && selectorFiles.length < 1) {
            return;
        } else {
            const filesArr = Array.prototype.slice.call(selectorFiles);
            let counter = 0;

            filesArr.forEach((img, index) => {
                const { name } = img;
                convert(img).then(v => {
                    filesArr[index] = ({ src: v as any, name: name.replace(name.substr(name.lastIndexOf('.')), ''), id: `${index + 1}` });
                    counter++;
                    if (counter === filesArr.length) {
                        SetProperty({
                            ...property,
                            form: {
                                ...form,
                                images: [
                                    ...form.images,
                                    ...filesArr
                                ]
                            }
                        });
                    }
                });
            });
        }
    }

    function updateImageText(id: any, text: string) {
        const updatedImages = images.map(image => {
            if (image.id === id) {
                image.name = text;
            }
            return image;
        })
        updateImages(updatedImages);
    }

    function removeImage(id: any) {
        const updatedImages = images.filter(image => image.id !== id);
        updateImages(updatedImages);
    }

    function makeImageMain(id: any) {
        const updatedImages = images.map((image, index) => {
            if (image.id === id) {
                image.id = '1';
            } else {
                image.id = `${index + 2}`
            }
            return image;
        })
        updateImages(updatedImages);
    }

    function updateImages(updatedImages: any[]) {
        SetProperty({
            ...property,
            form: {
                ...property.form,
                images: updatedImages
            }
        });
    }

    function convert(myFile: File): Promise<string | ArrayBuffer> {
        return new Promise<string | ArrayBuffer>((resolve, reject) => {
            const fileReader = new FileReader();
            if (fileReader && myFile) {
                fileReader.readAsDataURL(myFile);
                fileReader.onload = () => {
                    resolve(fileReader.result || '');
                };

                fileReader.onerror = (error) => {
                    reject(error);
                };
            } else {
                reject('No file provided');
            }
        });
    }

    function addListingCamera() {
        if (inputRef && inputRef.current) {
            inputRef.current.click();
        }
    }

    function inputChange(value: any, validity: boolean, field: any) {
        SetProperty({
            ...property,
            form: {
                ...property.form,
                [field]: value,
            },
            formInvalid: {
                ...property.formInvalid,
                [field]: validity,
            },
        });
    }

    function createListing() {
        props._appLoading(true);
        backendService.post('property/create', property.form).then((response: any) => {
            if (response.success) {
                setTimeout(() => {
                    SetProperty({ ...InitialState });
                }, 2000);
            }
            props._appLoading(false, {
                color: response.success ? 'success' : 'danger',
                message: response.message,
                showToast: true
            });
        })
    }

    function imageHeader(): React.ReactNode | null {
        if (images.length > 0) {
            return <IonCol size='12'><p>{languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'pictures')}</p></IonCol>;
        }
        return null;
    }

    function listImages(): React.ReactNode {
        return images.sort((a, b) => a.id - b.id).map((image, index) => (
            <IonCol key={image.id} size='12'>
                <div className='image-list-container'>
                    <img alt='property' src={image.src} />
                    <section>
                        <HighlightOff color='primary' onClick={() => removeImage(image.id)} />
                        {+index === 0 ? (
                            <FavoriteRounded color='error' onClick={() => makeImageMain(image.id)} />
                        ) : (
                                <FavoriteBorderRounded color='primary' onClick={() => makeImageMain(image.id)} />
                            )}
                    </section>
                </div>
                <IonInput maxlength={30} value={image.name} className='image-text' onIonChange={({ target: { value } }: any) => updateImageText(image.id, value)}>
                </IonInput>
            </IonCol>
        ));
    }

    return (
        <IonPage>
            <Header
                back={true}
                history={props.history}
                text={languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'add_listing')} />

            <IonContent className='add-listing'>
                <IonGrid>
                    <IonRow>
                        <IonCol size='12' sizeSm='8' pushSm='2' sizeXl='6' pushXl='3'>
                            <IonGrid>
                                <IonRow className='mt-2'>
                                    <IonCol size='12'>
                                        <Input
                                            rule='required'
                                            value={property.form.location}
                                            label={languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'address')}
                                            onChange={(value: any, validity: boolean) => inputChange(value, validity, 'location')} />
                                    </IonCol>
                                    <IonCol size='12'>
                                        <IonItem>
                                            <IonLabel>{languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'city')}</IonLabel>
                                            <IonSelect
                                                value={property.form.city}
                                                onIonChange={(e) => inputChange(e.detail.value, false, 'city')}
                                                cancelText={languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'cancel')}>
                                                {getCountryRegions(userCountry.country, props.countriesList).map((region: any) => (
                                                    <IonSelectOption key={region.name} value={region.name}>{region.name}</IonSelectOption>
                                                ))}
                                            </IonSelect>
                                        </IonItem>
                                    </IonCol>
                                    <IonCol size='12'>
                                        <Input
                                            type='number' rule='required'
                                            value={property.form.numOfRooms}
                                            label={languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'num_of_rooms')}
                                            onChange={(value: any, validity: boolean) => inputChange(value, validity, 'numOfRooms')} />
                                    </IonCol>
                                    <IonCol size='12'>
                                        <Input
                                            placeholder='120'
                                            type='number' rule='required'
                                            value={property.form.totalSpace}
                                            label={languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'total_space')}
                                            onChange={(value: any, validity: boolean) => inputChange(value, validity, 'totalSpace')} />
                                    </IonCol>
                                    <IonCol size='12'>
                                        <Input
                                            type='number' rule='required'
                                            value={property.form.rent}
                                            placeholder='1,0000'
                                            label={languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'rent')}
                                            onChange={(value: any, validity: boolean) => inputChange(value, validity, 'rent')} />
                                    </IonCol>
                                    <IonCol size='12'>
                                        <Select
                                            label='propertyType'
                                            options={[
                                                { value: 'rental', label: languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'rental') },
                                                { value: 'sales', label: languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'sales') },
                                            ]}
                                            value={property.form.advertType}
                                            onChange={(value: any) => inputChange(value, false, 'advertType')}
                                        />
                                    </IonCol>
                                    <IonCol size='12'>
                                        <Select
                                            label='paymentOptions'
                                            options={[
                                                { value: '1', label: languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'paymentOptions1') },
                                                { value: '6', label: languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'paymentOptions2') },
                                                { value: '12', label: languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'paymentOptions3') },
                                                { value: 'full', label: languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'full') },
                                            ]}
                                            value={property.form.paymentOptions}
                                            onChange={(value: any) => inputChange(value, false, 'paymentOptions')}
                                        />
                                    </IonCol>
                                    <IonCol size='12'>
                                        <Input
                                            textarea
                                            maxlength={60} rule='max-length'
                                            value={property.form.introText}
                                            label={languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'introText')}
                                            onChange={(value: any, validity: boolean) => inputChange(value, validity, 'introText')} />
                                    </IonCol>
                                    <IonCol size='12'>
                                        <JoditEditor
                                            value={property.form.generalDetails}
                                            config={{readonly: false }}
                                            onBlur={newContent => inputChange(newContent, false, 'generalDetails')} // preferred to use only this option to update the content for performance reasons
                                            onChange={newContent => { }}
                                        />
                                    </IonCol>

                                    {imageHeader()}
                                    <ThemeProvider theme={outerTheme}>
                                        {listImages()}
                                    </ThemeProvider>
                                    <IonCol size='12'>
                                        <AppButton
                                            onClick={() => createListing()}
                                            text={languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'submit')}
                                            disabled={isValid || property.pending || images.length === 0} />
                                    </IonCol>
                                </IonRow>
                            </IonGrid>
                            <input
                                multiple
                                type='file'
                                ref={inputRef}
                                accept='image/*'
                                className='hidden'
                                onChange={(e) => upload(e.target.files)} />
                        </IonCol>
                    </IonRow>
                </IonGrid>
                <IonFab horizontal='end' vertical='top' slot='fixed' edge>
                    <IonFabButton
                        color='dark'
                        disabled={property.pending}
                        onClick={() => addListingCamera()}>
                        <AddAPhoto />
                    </IonFabButton>
                </IonFab>
            </IonContent>
        </IonPage>
    );
}
