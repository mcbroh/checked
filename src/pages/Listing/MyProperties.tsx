import React, { useEffect, useState } from 'react';
import { IonPage, IonContent, IonGrid, IonRow, IonRefresher, IonRefresherContent } from '@ionic/react';

import { Header, Empty } from '../../components';
import { languageService, backendService } from '../../service';
import SingleProperty from './SingleProperty';
import { ListLoading } from '../../components/ListLoading';

export default function MyProperties(props: any)  {

    const [state, setState] = useState({
        listings: [],
        imgUrl: null,
        loading: true,
    });

    useEffect(() => {
        getListings();
    }, []);

    function getListings() {
        backendService.get('property/mine').then((response: any) => {
            setState({ loading: false, listings: response.data, imgUrl: response.url })
        })
    }

    function renderPage() {
        if (state.listings.length > 0) {
            return (
                <IonGrid>
                    <IonRow>
                        {state.listings.map((listing: any) => (
                            <SingleProperty
								key={listing._id}
								allowDelete
                                {...props}
                                listing={{ ...listing, imgUrl: state.imgUrl }} />
                        ))}
                    </IonRow>
                </IonGrid>

            );
        }
        return <Empty />;
    }

    return (
        <IonPage>
            <Header
                back
                history={props.history}
                text={languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'myAdverts')} />
            <IonContent className="home-listings">
                <IonRefresher slot="fixed" >
                    <IonRefresherContent> </IonRefresherContent>
                </IonRefresher>
                {state.loading ? <ListLoading /> : renderPage()}
            </IonContent>
        </IonPage>
    );
};
