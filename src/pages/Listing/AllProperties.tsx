import React, { useEffect, useState } from 'react';
import { IonPage, IonContent, IonGrid, IonRow, IonRefresher, IonRefresherContent } from '@ionic/react';
import { RefresherEventDetail } from '@ionic/core';

import { Header, Empty } from '../../components';
import { languageService, backendService } from '../../service';
import SingleProperty from './SingleProperty';
import { ListLoading } from '../../components/ListLoading';

export default function AllProperties(props: any) {

    const [state, setState] = useState({
        listings: [],
        imgUrl: null,
        loading: true,
    });

    useEffect(() => {
        getListings();
    }, []);


    function getListings(event: any = null) {
        const country = JSON.parse(localStorage.getItem('USER_PRESET') as any).country;
        backendService.get('property/country/' + country).then((response: any) => {
            if (event) {
                event.detail.complete();
            }
            setState({ loading: false, listings: response.data, imgUrl: response.url });
        });
    }

    function renderPage() {
        if (state.listings.length > 0) {
            return (
                <IonGrid>
                    <IonRow>
                        {state.listings.map((listing: any) => (
                            <SingleProperty
                                key={listing._id}
                                {...props}
                                listing={{ ...listing, imgUrl: state.imgUrl }} />
                        ))}
                    </IonRow>
                </IonGrid>

            );
        }
        return <Empty />;
    }

    return (
        <IonPage>
            <Header
                text={languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'propertiesForRent')} />
            <IonContent className="home-listings">
                <IonRefresher slot="fixed" onIonRefresh={(e: CustomEvent<RefresherEventDetail>) => getListings(e)}>
                    <IonRefresherContent> </IonRefresherContent>
                </IonRefresher>
                {state.loading ? <ListLoading /> : renderPage()}
            </IonContent>
        </IonPage>
    )
};
