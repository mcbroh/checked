import React from 'react';
import {
    IonPage, IonContent, IonGrid, IonRow,
    IonCol, IonCard, IonIcon, IonFab, IonFabButton,
    withIonLifeCycle, IonFabList
} from '@ionic/react';

import { Header, NotFound, HomeListLoading } from '../../components';
import { languageService, backendService } from '../../service';
import { call, cash, home, bed, arrowUp, shareAlt, trash } from 'ionicons/icons';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import { createMarkup } from '../../helpers';

class SelectedList extends React.Component<any, any> {

    constructor(props: any) {
        super(props);
        this.state = {
            imgUrl: null,
            property: null,
            loading: true,
            showImage: JSON.parse(localStorage.getItem('USER_PRESET') as any)['displayImages'],
        };
    }

    ionViewDidEnter() {
        const { history: { location: { state } }, match: { params: { id } } } = this.props;
        let allowDelete = false;
        if (state) {
            allowDelete = state.allowDelete;
        }

        this.setState({
            id,
            allowDelete,
        }, () => { this.getProperty(); });
    }

    ionViewWillLeave() {
        this.setState({
            loading: true,
            property: null,
            id: null
        })
    }

    getProperty() {
        backendService.get('property/' + this.state.id).then((response: any) => {
            const property = response.success ? response.data : null;
            this.setState({ property: property, loading: false, imgUrl: response.url || null });
        });
    }

    deleteProperty() {
        this.props._appLoading(true);
        backendService.post('property/delete', { id: this.state.id }).then((response: any) => {
            if (response.success) {
                this.props.history.goBack();
            }
            this.props._appLoading(false, {
                color: response.success ? 'success' : 'danger',
                message: response.message,
                showToast: true
            });
        })
    }

    private renderProperty(property: any, allowDelete: boolean, imgUrl: string): React.ReactNode {
        return <>
            <Header history={this.props.history} back={true} text={property.location} />
            <IonContent className="selected-home">
                {this.state.showImage && <div className="header-image" style={{ backgroundImage: `url(${imgUrl + property.mainImage})` }} />}
                <div className='details'>
                    <div>
                        <IonIcon slot="end" icon={bed}></IonIcon>
                        <span>{property.numOfRooms}</span>
                    </div>
                    <div>
                        <IonIcon slot="end" icon={home}></IonIcon>
                        <span>{property.totalSpace} m<sup>2</sup></span>
                    </div>
                    <div>
                        <IonIcon slot="end" icon={cash}></IonIcon>
                        <span>{property.rent}</span>
                    </div>
                </div>
                <IonGrid>
                    <div dangerouslySetInnerHTML={createMarkup(property.generalDetails)} />
                    {this.state.showImage && <IonRow>
                        {property.images.map((image: any, index: any) => (<IonCol key={index} size="12" sizeMd="6">
                            <IonCard>
                                <img src={imgUrl + image.key} alt={property.location} />
                            </IonCard>
                            <p className="image-name">{image.name}</p>
                        </IonCol>))}
                    </IonRow>}
                    <h6 className="company-name">{languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'advertiser')}: {property.advertiser.name}</h6>
                </IonGrid>
                <IonFab horizontal="end" vertical="bottom" slot="fixed">
                    <IonFabButton color="dark">
                        <IonIcon icon={arrowUp} />
                    </IonFabButton>
                    <IonFabList side="top">
                        <IonFabButton color="light" href={"tel:" + property.advertiser.phone}>
                            <IonIcon icon={call} />
                        </IonFabButton>
                        <IonFabButton color="light" onClick={() => this.openSMSMobile()}>
                            <IonIcon icon={shareAlt} />
                        </IonFabButton>
                        {allowDelete && (
                            <IonFabButton color="danger" onClick={() => this.deleteProperty()}>
                                <IonIcon icon={trash} />
                            </IonFabButton>
                        )}
                    </IonFabList>
                    {!this.state.showImage && (<IonFabList side="start">
                        <FormControlLabel
                            className="showImage"
                            control={
                                <Switch
                                    checked={this.state.showImage}
                                    onChange={() => this.setState({ showImage: true })}
                                    value="checkedB"
                                    color="primary"
                                />
                            }
                            label={languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'loadImages')}
                        />
                    </IonFabList>
                    )}
                </IonFab>
            </IonContent>
        </>;
    }

    openSMSMobile() {
        window.open('sms:1&body=' + window.location.href, '_self');
        return false;
    }

    render() {
        const { property, loading, allowDelete, imgUrl } = this.state;

        return (
            <IonPage>
                {loading ? <HomeListLoading /> : (
                    property ? this.renderProperty(property, allowDelete, imgUrl) : <NotFound />
                )}
            </IonPage>

        )
    }

};

export default withIonLifeCycle(SelectedList);
