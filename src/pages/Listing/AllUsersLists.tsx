import React, { useState } from 'react';
import {
    IonPage, IonContent, IonItemSliding, IonItemOptions,
    IonItemOption, IonItem, IonLabel, IonIcon, IonActionSheet
} from '@ionic/react';

import { Header } from '../../components';
import { languageService, backendService } from '../../service';
import { more, create } from 'ionicons/icons';

type State = {
    showVerify: boolean;
    employees: Array<any>;
}

const AllUsersLists: React.FC<any> = (props) => {

    let headerText = '';
    let storageArray = '';
    let showVerify = false;
    const { match: { params: { type } } } = props;

    switch (type) {
        case 'employee_list':
            headerText = 'EmployeesList';
            storageArray = 'EmployeesList';
            break;
        case 'new_companies':
            showVerify = true;
            headerText = 'new_companies';
            storageArray = 'UnVerifiedCompaniesList';
            break;
        case 'companies':
            headerText = 'companies';
            storageArray = 'VerifiedCompaniesList';
            break;
        default:
            break;
    }

    const [state, setState] = useState<State>({
        showVerify: showVerify,
        employees: JSON.parse((localStorage.getItem(storageArray) as any)),
    });
    const [selectedUser, setSelectedUser] = useState<any>(null);
    const [showActionSheet, setShowActionSheet] = useState(false);

    function deleteUser() {
        props._appLoading(true);
        backendService.patch('companies', { user: selectedUser }).then(response => {
            if (response.success) {
                setState({
                    ...state,
                    employees: state.employees.filter((employee: any) => employee._id !== selectedUser._id)
                })
            };
            props._appLoading(false, {
                color: response.success ? 'success' : 'danger',
                message: response.message || languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'success'),
                showToast: true
            });
        })
    }

    function verifyUser(selectedUser: any) {
        props._appLoading(true);
        backendService.post('companies/application/grant', { userId: selectedUser._id }).then(response => {
            if (response.success) {
                setState({
                    ...state,
                    employees: state.employees.filter((employee: any) => employee._id !== selectedUser._id)
                })
            };
            props._appLoading(false, {
                color: response.success ? 'success' : 'danger',
                message: response.message || languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'success'),
                showToast: true
            });
        })
    }

    function moreOptions() {
        const main = [{
            text: languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'delete'),
            icon: 'trash',
            role: 'destructive',
            handler: () => {
                deleteUser();
            }
        }, {
            text: languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'cancel'),
            icon: 'close',
            role: 'cancel',
        }];
        const needVerify = [{
            text: languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'call'),
            icon: 'phone',
            handler: () => window.location.href = "tel:" + selectedUser.company.phone,
        }, {
            text: languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'verify'),
            icon: 'checkmark',
            handler: () => verifyUser(selectedUser),
        }]
        return state.showVerify ? [...main, ...needVerify] : main;
    }
    return (
        <IonPage>
            <Header
                back={true}
                history={props.history}
                text={languageService.getText(localStorage.getItem('USER_LANGUAGE'), headerText)} />

            <IonContent>
                {
                    state.employees.map((employee: any) => (
                        <IonItemSliding key={employee._id} onIonDrag={() => setSelectedUser(employee)}>
                            <IonItem lines="full">
                                <IonLabel>
                                    {employee.name}
                                </IonLabel>
                            </IonItem>
                            <IonItemOptions>
                                <IonItemOption color="dark" onClick={() => setShowActionSheet(true)}>
                                    <IonIcon slot="end" icon={more}></IonIcon>
                                    {languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'more')}
                                </IonItemOption>
                                {!state.showVerify ?
                                    <IonItemOption color="primary" disabled>
                                        <IonIcon slot="end" icon={create}></IonIcon>
                                        {languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'updated')}
                                    </IonItemOption>
                                    : null}
                            </IonItemOptions>
                        </IonItemSliding>
                    ))
                }
                <IonActionSheet
                    isOpen={showActionSheet}
                    onDidDismiss={() => setShowActionSheet(false)}
                    buttons={moreOptions() as any}
                >
                </IonActionSheet>
            </IonContent>
        </IonPage>
    )
};

export default AllUsersLists;
