import React from 'react';
import { IonCol } from '@ionic/react';
import { languageService } from '../../service';

const SingleProperty: React.FC<any> = ({ history, listing, allowDelete }) => {
    const canShowImage = JSON.parse(localStorage.getItem('USER_PRESET') as any)['displayImages'];
    return (
        <IonCol size='12'>
            <div
                className="single-property-list"
                onClick={() => { history.push('/property/' + listing._id, { id: listing._id, allowDelete: Boolean(allowDelete) }); }}>
                {canShowImage &&
                    <section className="image-holder">
                        <img src={listing.imgUrl + listing.mainImage} alt={listing.location} />
                    </section>
                }


                <section className="details-holder">
                    <div className="header">
                        <div>
                            <p>{languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'broker')}</p>
                        </div>
                        <p>{listing.advertiser.name}</p>
                    </div>
                    <h3>{listing.location}</h3>

                    <p className="row-details">
                        <span>{languageService.getText(localStorage.getItem('USER_LANGUAGE'), listing.advertType)}</span>
                        <span>{listing.numOfRooms} {languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'rooms')}</span>
                        <span>{listing.totalSpace} m<sup>2</sup></span>
                    </p>

                    <h3 className="rent">{listing.rent} {listing.currency}</h3>
                </section>
            </div>
        </IonCol>
    )
};

export default SingleProperty;
