import React, { useState } from 'react';
import { IonPage, IonContent, useIonViewDidEnter } from '@ionic/react';
import { Transition } from 'react-transition-group';

import { Header } from '../components';
import { languageService } from '../service';

const duration = 3000;

const defaultStyle = {
    transition: `opacity ${duration}ms ease-in-out`,
    opacity: 0,
}

const transitionStyles: { [index: string]: object } = {
    entering: { opacity: 1 },
    entered: { opacity: 1 },
    exiting: { opacity: 0 },
    exited: { opacity: 0 },
};

const views = {
    view1: false,
    view2: false,
    view3: false,
    view4: false,
}

const About: React.FC = ({history}: any) => {

    const [inProps, setInProps] = useState({
        agents: views,
        users: views,
    });

    useIonViewDidEnter(() => {
            setInProps({
                ...inProps,
                agents: {
                    ...inProps.agents,
                    view1: true
                }
            });
    });

    function setAgents(view: string) {
        setInProps({
            ...inProps,
            agents: {
                ...inProps.agents,
                [view]: true
            }
        });
    }

    function setUsers(view: string) {
        setInProps({
            ...inProps,
            agents: views,
            users: {
                ...inProps.users,
                [view]: true
            }
        });
    }

    const isLoggedIn =  Boolean(localStorage.getItem("TOKEN"));

    return (
        <IonPage>
            <Header />
            <IonContent>
                <section className="about">
                    <Transition in={inProps.users.view1} timeout={duration} onEntered={() => setUsers('view2')}>
                        {state => (
                            <div style={{
                                ...defaultStyle,
                                ...transitionStyles[state]
                            }}>
                                {languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'aboutUser')}
                        </div>
                        )}
                    </Transition>
                    <Transition in={inProps.users.view2} timeout={duration} onEntered={() => setUsers('view3')}>
                        {state => (
                            <div style={{
                                ...defaultStyle,
                                ...transitionStyles[state]
                            }}>
                                {languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'aboutTip')}
                        </div>
                        )}
                    </Transition>
                    <Transition in={inProps.users.view3} timeout={duration} onEntered={() => setUsers('view4')}>
                        {state => (
                            <div style={{
                                ...defaultStyle,
                                ...transitionStyles[state]
                            }}>
                                {languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'aboutContactAgents')}
                        </div>
                        )}
                    </Transition>

                    <Transition in={inProps.agents.view1} timeout={duration} onEntered={() => setAgents('view2')}>
                        {state => (
                            <div style={{
                                ...defaultStyle,
                                ...transitionStyles[state]
                            }}>
                                {languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'aboutTakePic')}
                        </div>
                        )}
                    </Transition>
                    <Transition in={inProps.agents.view2} timeout={duration} onEntered={() => setAgents('view3')}>
                        {state => (
                            <div style={{
                                ...defaultStyle,
                                ...transitionStyles[state]
                            }}>
                                {languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'aboutAddDetails')}
                        </div>
                        )}
                    </Transition>
                    <Transition in={inProps.agents.view3} timeout={duration} onEntered={() => setUsers('view1')}>
                        {state => (
                            <div style={{
                                ...defaultStyle,
                                ...transitionStyles[state]
                            }}>
                                {languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'aboutAdvertiseFree')}
                        </div>
                        )}
                    </Transition>
                    <Transition in={inProps.users.view4} timeout={duration}>
                        {state => (
                            <div className="pointer" style={{
                                ...defaultStyle,
                                ...transitionStyles[state]
                            }}
                            onClick={() => {
                                isLoggedIn ? history.goBack() : history.push('/register')
                            }}>
                                {languageService.getText(localStorage.getItem('USER_LANGUAGE'), isLoggedIn ? 'back' : 'go_to_register')}
                        </div>
                        )}
                    </Transition>
                </section>
            </IonContent>
        </IonPage>
    );
}

export default About;
