import React from 'react';
import { IonPage, IonContent, IonItem, IonLabel, IonList, withIonLifeCycle } from '@ionic/react';

import { Header } from '../../components';
import { languageService, backendService } from '../../service';

const levels = {
    SUPER_ADMIN: 'superAdmin',
    SUB_SUPER_ADMIN: 'subSuperAdmin',
    ADMIN: 'admin',
    SUB_ADMIN: 'subAdmin',
    ODINARY_USER: 'ordinaryUser'
};

class Settings extends React.Component<any, any> {
    isLoggedIn = localStorage.getItem("TOKEN");
    currentUser = JSON.parse(localStorage.getItem('USER') as any);

    ionViewDidEnter() {
        this.isLoggedIn && backendService.get('companies').then(response => {
            if (response.success) {
                const users = response.users.filter((user: any) => user.username !== this.currentUser['username']);

                const EmployeesList = users.filter((user: any) => (user.privilege === levels.SUB_SUPER_ADMIN || user.privilege === levels.SUB_ADMIN));
                const VerifiedCompaniesList = users.filter((user: any) => user.privilege === levels.ADMIN && user.company.verified);
                const UnVerifiedCompaniesList = users.filter((user: any) => user.privilege === levels.ADMIN && !user.company.verified);
                /* const DataSource4 = users.filter((user: any) => user.privilege === levels.ODINARY_USER); */

                localStorage.setItem('EmployeesList', JSON.stringify(EmployeesList));
                localStorage.setItem('VerifiedCompaniesList', JSON.stringify(VerifiedCompaniesList));
                localStorage.setItem('UnVerifiedCompaniesList', JSON.stringify(UnVerifiedCompaniesList));
            }
        });
    };

    renderVerifyCompanies(): React.ReactNode {
        return (
            <>
                <IonItem detail lines='full' onClick={() => {
                    this.props.history.push('/employees/list/companies');
                }}>
                    <IonLabel>
                        {languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'companies')}
                    </IonLabel>
                </IonItem>
                <IonItem detail lines='full' onClick={() => {
                    this.props.history.push('/employees/list/new_companies');
                }}>
                    <IonLabel>
                        {languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'new_companies')}
                    </IonLabel>
                </IonItem>
            </>
        );
    };

    renderLoggedIn() {
        return (
            <>
                <IonItem
                    detail
                    lines='full'
                    onClick={() => {
                        this.props.history.push('/employees/new')
                    }}>
                    <IonLabel>
                        {languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'new_employee')}
                    </IonLabel>
                </IonItem>
                <IonItem
                    detail
                    lines='full'
                    onClick={() => {
                        this.props.history.push('/employees/list/employee_list')
                    }}>
                    <IonLabel>
                        {languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'employee_list')}
                    </IonLabel>
                </IonItem>

                {
                    (this.currentUser.privilege === levels.SUPER_ADMIN ||
                        this.currentUser.privilege === levels.SUB_SUPER_ADMIN) &&
                    this.renderVerifyCompanies()
                }
            </>
        );
    }


    render() {

        return (
            <IonPage>
                <Header
                    history={this.props.history}
                    text={languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'settings')} translucent />
                <IonContent>
                    <IonList>
                        {this.isLoggedIn && this.renderLoggedIn()}
                        <IonItem
                            detail
                            lines='none'
                            disabled
                            onClick={() => {
                                // console.log('touch');
                            }}>
                            <IonLabel>
                                {languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'contact_us')}
                            </IonLabel>
                        </IonItem>
                        <IonItem
                            detail
                            lines='none'
                            onClick={() => this.props.resetSettings()}>
                            <IonLabel>
                                {languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'userSettings')}
                            </IonLabel>
                        </IonItem>
                        {this.isLoggedIn && (
                            <IonItem
                                lines='full'
                                color='danger'
                                onClick={() => {
                                    const event = new CustomEvent('loggedIn', {
                                        detail: false,
                                    });
                                    window.dispatchEvent(event);
                                }}>
                                <IonLabel>
                                    {languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'signOut')}
                                </IonLabel>
                            </IonItem>
                        )}
                    </IonList>
                </IonContent>
            </IonPage>
        );
    }
};

export default withIonLifeCycle(Settings);
