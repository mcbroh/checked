import React, { useState } from 'react';
import { IonPage, IonContent, IonGrid, IonRow, IonCol } from '@ionic/react';

import { Header, Input, AppButton } from '../../components';
import { languageService, backendService } from '../../service';

const AddEmployee: React.FC = ({ _appLoading, history }: any) => {

    const [formInvalid, setFormInvalid] = useState({
        name: true,
        email: true,
        username: true,
    });
    const [form, setForm] = useState({
        name: '',
        email: '',
        username: '',
    });
    const isValid = Object.values(formInvalid).includes(true)

    function inputChange(value: any, validity: boolean, field: string) {
        setTimeout(() => {
            setFormInvalid({
                ...formInvalid,
                [field]: validity
            })
        }, 1);
        setForm({
            ...form,
            [field]: value
        })
    }

    function submitForm() {
        _appLoading(true)
        backendService.post('companies/user', form).then(response => {
            if (response.success) {
                setForm({
                    name: '',
                    email: '',
                    username: '',
                })
            };
            _appLoading(false, {
                color: response.success ? 'success' : 'danger',
                message: response.message || languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'password_reset_success'),
                showToast: true
            });
        })
    }


    return (
        <IonPage>
            <Header
                back={true}
                history={history}
                text={languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'new_employee')} />

            <IonContent>
                <IonGrid>
                    <IonRow>
                        <IonCol size='12'>
                            <Input
                                value={form.name}
                                label={languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'name')}
                                rule='required'
                                onChange={(value: any, validity: boolean) => inputChange(value, validity, 'name')}
                            />
                        </IonCol><IonCol size='12'>
                            <Input
                                value={form.email}
                                label={languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'email')}
                                type='email'
                                rule='email'
                                onChange={(value: any, validity: boolean) => inputChange(value, validity, 'email')}
                            />
                        </IonCol>
                        <IonCol size='12'>
                            <Input
                                value={form.username}
                                label={languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'username')}
                                rule='required'
                                onChange={(value: any, validity: boolean) => inputChange(value, validity, 'username')}
                            />
                        </IonCol>
                        <IonCol size='12'>
                            <AppButton
                                text={languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'submit')}
                                disabled={isValid}
                                onClick={() => submitForm()} />
                        </IonCol>
                    </IonRow>
                </IonGrid>
            </IonContent>
        </IonPage>
    )
};

export default AddEmployee;
