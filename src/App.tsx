import React from 'react';
import { Redirect, Route } from 'react-router-dom';
import { IonApp, IonSplitPane, IonLoading, IonRouterOutlet, IonAlert } from '@ionic/react';
import { IonReactRouter } from '@ionic/react-router';
import { home, cog, save, logIn, create, apps, informationCircleOutline } from 'ionicons/icons';

import Login from './pages/portal/Login';
import RecoverPassword from './pages/portal/RecoverPassword';
import ResetPassword from './pages/portal/ResetPassword';
import { languageService, backendService } from './service';
import { AppPage } from './declarations';
import Register from './pages/portal/Register';

/* Core CSS required for Ionic components to work properly */
import '@ionic/react/css/core.css';

/* Basic CSS for apps built with Ionic */
import '@ionic/react/css/normalize.css';
import '@ionic/react/css/structure.css';
import '@ionic/react/css/typography.css';

/* Optional CSS utils that can be commented out */
import '@ionic/react/css/padding.css';
import '@ionic/react/css/float-elements.css';
import '@ionic/react/css/text-alignment.css';
import '@ionic/react/css/text-transformation.css';
import '@ionic/react/css/flex-utils.css';
import '@ionic/react/css/display.css';

/* Theme variables */
import './theme/variables.css';
import './styles/main.scss';

import Settings from './pages/Settings/Settings';
import AllUsersLists from './pages/Listing/AllUsersLists';
import AddEmployee from './pages/Settings/AddEmployee';
import { AddListing } from './pages/Listing/AddListing';
import AllProperties from './pages/Listing/AllProperties';
import MyProperties from './pages/Listing/MyProperties';
import SelectedList from './pages/Listing/SelectedList';
import About from './pages/About';
import SideMenu from './components/SideMenu';
import { Toast } from './components';
import { InstallPrompt } from './components/InstallPrompt';
import Slide from './components/Slide';
import Splash from './components/Splash';


declare global {
    interface Window {
        language: 'french' | 'english';
        transactionStatusArr: {};
    }
}

interface AppHasMessage {
    message: string;
    showToast: boolean;
    color: any;
}

interface State {
    language: string;
    appReady: boolean;
    userPreset: boolean;
    isLoggedIn: boolean;
    showLoader: boolean;
    bannerVisible: boolean;
    countriesList: any;
    hasMessage: AppHasMessage;
    showUpdateAlert: boolean;
}

class App extends React.Component<any, State> {
    state: State;
    retry = 0;
    appPages: AppPage[];
    deferredPrompt: any;

    constructor(props: any) {
        super(props);
        if (!localStorage.getItem('USER_LANGUAGE')) {
            localStorage.setItem('USER_LANGUAGE', 'french');
        }

        this.appPages = [
            { title: languageService.getText(localStorage.getItem('USER_LANGUAGE'), '_properties'), path: '/properties', icon: home, loggedIn: null },
            { title: languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'companies'), loggedIn: false },
            { title: languageService.getText(localStorage.getItem('USER_LANGUAGE'), '_login'), path: '/login', icon: logIn, loggedIn: false },
            { title: languageService.getText(localStorage.getItem('USER_LANGUAGE'), '_register'), path: '/register', icon: save, loggedIn: false },
            { title: languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'advertise'), loggedIn: true, privilege: true },
            { title: languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'add_listing'), path: '/properties/add', icon: create, loggedIn: true, privilege: true },
            { title: languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'my_listings'), path: '/properties/mine', icon: apps, loggedIn: true, privilege: true },
            { title: languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'extra'), loggedIn: null },
            { title: languageService.getText(localStorage.getItem('USER_LANGUAGE'), '_settings'), path: '/settings', icon: cog, loggedIn: null },
            { title: languageService.getText(localStorage.getItem('USER_LANGUAGE'), '_about'), path: '/about', icon: informationCircleOutline, loggedIn: null },
        ];
        this.state = {
            appReady: false,
            showLoader: false,
            bannerVisible: false,
            showUpdateAlert: false,
            countriesList: [],
            hasMessage: {
                message: '',
                color: undefined,
                showToast: false,
            },
            isLoggedIn: Boolean(localStorage.getItem("TOKEN")),
            userPreset: Boolean(localStorage.getItem('USER_PRESET')),
            language: localStorage.getItem('USER_LANGUAGE') as string,
        };
        this.getDataConfigFromServer();
        this.initializeAuthStateListener();
        this.triggerAppInstall();
    }

    private getDataConfigFromServer() {
        const appConfig = JSON.parse(localStorage.getItem('APP_CONFIG') as string) || {};

        backendService.get('config').then(({ success, data }: any) => {
            if (success) {
                localStorage.setItem('APP_CONFIG', JSON.stringify(data));
                this.setState({
                    appReady: true,
                    countriesList: data.countriesList,
                    showUpdateAlert: appConfig['appVersion'] ? appConfig['appVersion'] !== data['appVersion'] : false,
                });
            } else {
                if (this.retry <= 3) {
                    this.getDataConfigFromServer();
                    this.retry++
                }
            }
        });
    }

    private initializeAuthStateListener() {
        window.addEventListener('loggedIn', (event: any) => {
            this.setState({
                isLoggedIn: event.detail,
            }, () => {
                if (event.detail === false) {
                    localStorage.removeItem('TOKEN');
                    localStorage.removeItem('USER');
                    localStorage.removeItem('UsersList');
                    window.location.href = '/login';
                } else {
                    window.location.href = '/';
                }
            });
        });
    }

    private triggerAppInstall() {
        window.addEventListener('beforeinstallprompt', (e) => {
            // Prevent Chrome 67 and earlier from automatically showing the prompt
            e.preventDefault();
            // Stash the event so it can be triggered later.
            this.deferredPrompt = e;
            // Update UI notify the user they can add to home screen
            this.setState({ bannerVisible: true });
        });
    }

    private showInstallBanner() {
        if (this.deferredPrompt !== undefined && this.deferredPrompt !== null) {
            // Show the prompt
            this.deferredPrompt.prompt();
            // Wait for the user to respond to the prompt
            this.deferredPrompt.userChoice
                .then((choiceResult: any) => {
                    if (choiceResult.outcome === 'accepted') {
                        console.log('User accepted the A2HS prompt');
                    } else {
                        console.log('User dismissed the A2HS prompt');
                    }
                    // We no longer need the prompt.  Clear it up.
                    this.deferredPrompt = null;
                });
        }
    }

    _appLoading(state: boolean, message: AppHasMessage | undefined) {
        if (message) {
            this.setState({
                showLoader: state,
                hasMessage: message,
            });
        } else {
            this.setState({
                showLoader: state,
            });
        }
    }

    renderAuthRoute(element: JSX.Element): JSX.Element {
        if (this.state.isLoggedIn) {
            return element
        }
        return <Login _appLoading={(state: boolean, message: AppHasMessage) => this._appLoading(state, message)} />
    }

    renderUnAuthRoute(element: JSX.Element): JSX.Element {
        if (!this.state.isLoggedIn) {
            return element
        }
        return <AllProperties _appLoading={(state: boolean, message: AppHasMessage) => this._appLoading(state, message)} />
    }

    renderPages() {
        return (
            <IonReactRouter>
                <IonSplitPane contentId="main">
                    <SideMenu
                        appPages={this.appPages}
                        isLoggedIn={this.state.isLoggedIn} />
                    <IonRouterOutlet id="main">
                        <Route exact={true} path="/login" render={(props: any) => this.renderUnAuthRoute(
                            <Login {...props} _appLoading={(state: boolean, message: AppHasMessage) => this._appLoading(state, message)} />)}
                        />
                        <Route exact={true} path="/register" render={(props: any) => this.renderUnAuthRoute(
                            <Register {...props} _appLoading={(state: boolean, message: AppHasMessage) => this._appLoading(state, message)} />)}
                        />
                        <Route exact={true} path="/password/recover" render={(props: any) => this.renderUnAuthRoute(
                            <RecoverPassword {...props} _appLoading={(state: boolean, message: AppHasMessage) => this._appLoading(state, message)} />)}
                        />
                        <Route exact={true} path="/password/reset/:userId/:token" render={(props: any) => this.renderUnAuthRoute(
                            <ResetPassword {...props} _appLoading={(state: boolean, message: AppHasMessage) => this._appLoading(state, message)} />)}
                        />
                        <Route exact={true} path="/properties" render={(props: any) => (
                            <AllProperties {...props} _appLoading={(state: boolean, message: AppHasMessage) => this._appLoading(state, message)} />)}
                        />
                        <Route exact={true} path="/properties/add" render={(props: any) => this.renderAuthRoute(
                            <AddListing {...props} countriesList={this.state.countriesList} _appLoading={(state: boolean, message: AppHasMessage) => this._appLoading(state, message)} />)}
                        />
                        <Route exact={true} path="/properties/mine" render={(props: any) => this.renderAuthRoute(
                            <MyProperties {...props} _appLoading={(state: boolean, message: AppHasMessage) => this._appLoading(state, message)} />)}
                        />
                        <Route exact={true} path="/property/:id" render={(props: any) => (
                            <SelectedList {...props} _appLoading={(state: boolean, message: AppHasMessage) => this._appLoading(state, message)} />)}
                        />
                        <Route exact={true} path="/settings" render={(props: any) => (
                            <Settings {...props}
                                resetSettings={() => this.setState({ userPreset: false })}
                                _appLoading={(state: boolean, message: AppHasMessage) => this._appLoading(state, message)} />)}
                        />
                        <Route exact={true} path="/employees/list/:type" render={(props: any) => this.renderAuthRoute(
                            <AllUsersLists {...props} _appLoading={(state: boolean, message: AppHasMessage) => this._appLoading(state, message)} />)}
                        />
                        <Route exact={true} path="/employees/new" render={(props: any) => this.renderAuthRoute(
                            <AddEmployee {...props} _appLoading={(state: boolean, message: AppHasMessage) => this._appLoading(state, message)} />)}
                        />
                        <Route exact={true} path="/about" render={(props: any) => (
                            <About {...props} _appLoading={(state: boolean, message: AppHasMessage) => this._appLoading(state, message)} />)}
                        />
                        <Route exact={true} path="/" render={() => <Redirect to="/properties" />} />
                    </IonRouterOutlet>
                </IonSplitPane>
            </IonReactRouter>
        )
    }

    renderIntroPage() {
        return <Slide
            language={this.state.language}
            countriesList={this.state.countriesList}
            updateLanguage={(value: string) => this.setState({ language: value })} />
    }

    renderAppReadyPages() {
        if (this.state.userPreset) {
            return this.renderPages();
        }
        return this.renderIntroPage();
    }

    render() {
        const { bannerVisible, showLoader, appReady, showUpdateAlert, hasMessage: { message, showToast, color } } = this.state;
        return (
            <IonApp>
                {appReady ? this.renderAppReadyPages() : <Splash />}

                {bannerVisible && <InstallPrompt showInstallBanner={() => this.showInstallBanner()} />}
                <IonLoading isOpen={showLoader} />
                <Toast
                    color={color}
                    display={showToast}
                    message={message || languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'success')}
                    toastHidden={() => this.setState({ hasMessage: { ...this.state.hasMessage, showToast: false } })} />
                <IonAlert
                    isOpen={showUpdateAlert}
                    onDidDismiss={() => this.setState({showUpdateAlert: false})}
                    header={languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'updateAppAvailable')}
                    message={languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'updateAppMsg')}
                    buttons={[
                        {
                          text: languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'cancel'),
                          role: 'cancel',
                          handler: () => { }
                        },
                        {
                          text: 'Ok!',
                          handler: () => {
                            window.location.reload();
                          }
                        }
                      ]}
                />
            </IonApp>
        );
    }
}

export default App;
