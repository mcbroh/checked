export function validateField(rule: string = '', minlength: number = 0, maxlength: number = 0, value: string): { hasError: boolean; errorMsg: string } {
    const error = {
        hasError: false,
        errorMsg: '',
    };
    let invalidState = false;
    switch (rule) {
        case 'email':
            invalidState = !value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
            error.errorMsg = invalidState ? 'Email is invalid' : '';
            break;
        case 'password':
            invalidState = value.length < 6;
            error.errorMsg = invalidState ? 'Password is too short' : '';
            break;
        case 'min-length':
            invalidState = value.length < minlength;
            error.errorMsg = invalidState ? 'It should be at least ' + minlength + ' characters long' : '';
            break;
        case 'max-length':
            invalidState = value.length > maxlength;
            error.errorMsg = invalidState ? 'Maximum allowed charaters is ' + maxlength : '';
            break;
        case 'required':
            invalidState = value === null || value === '' || value.length < 1;
            error.errorMsg = invalidState ? 'Field required!' : '';
            break;
        default:
            break;
    }
    error.hasError = invalidState;
    return error;
}
