export function getCountryRegions(argCountry: string | null, countriesList: Array<any>) {
    const selectedCountry = countriesList.filter((country: any) => country.code === argCountry)[0];
    return selectedCountry ? selectedCountry.region : [];
}
