import React from 'react';
import { IonButton } from '@ionic/react';
import { languageService } from '../service';

type Props = {
	showInstallBanner: () => void;
}

export const InstallPrompt: React.FC<Props> = ({showInstallBanner}: Props) => (
    <section className="app-installer-container">
        <p>{languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'installMessage')}</p>
        <IonButton
            color="dark"
            onClick={() => showInstallBanner()}>{languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'install')}</IonButton>
    </section>
)
