import React from 'react';
import { IonItem, IonLabel, IonSelect, IonSelectOption } from '@ionic/react';

import { languageService } from '../service';

interface Props {
    value: any;
    label: string;
    options: {value: string; label: string}[];
    disabled?: boolean;
    placeholder?: string;
    onChange: (value: any) => void;
}

export function Select({ value, label, options, onChange }: Props) {
    return (
        <>
            <IonItem>
                <IonLabel>{languageService.getText(localStorage.getItem('USER_LANGUAGE'), label)}</IonLabel>
                <IonSelect
                    value={value}
                    interface='action-sheet'
                    cancelText={languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'cancel')}
                    onIonChange={(e) => onChange(e.detail.value)}>
                    {options.map((option: any) => (
                        <IonSelectOption key={option.value} value={option.value}>{option.label}</IonSelectOption>
                    ))}
                </IonSelect>
            </IonItem>
        </>
    )
}
