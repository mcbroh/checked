import React from 'react';
import { IonPicker } from "@ionic/react";

import { languageService } from '../service';
import { PickerProps } from '.';

export const Picker = ({ name, isOpen, options, onSelect, onCancel }: PickerProps) => (
	<IonPicker
		isOpen={isOpen}
		columns={[{ name: name, options: options }]}
		buttons={[
			{
				text: languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'cancel'),
				role: 'cancel',
				handler: () => {
					onCancel();
				}
			},
			{
				text: languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'confirm'),
				handler: (selected) => {
					onSelect(name, selected[Object.keys(selected)[0]].value);
				}
			}
		]}
	></IonPicker>
)
