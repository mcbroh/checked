import { IonCard, IonCardHeader, IonCardSubtitle, IonCardTitle, IonCardContent } from "@ionic/react";
import React, { ReactNode } from 'react';
interface Props {
	className: string;
	image?: string;
	title: string;
	subtitle?: string;
	children: ReactNode
}

export const Card = ({ className, image, subtitle, title, children }: Props) => (
	<IonCard className={className}>
		{image ? <img src={image} alt="" /> : null}
		<IonCardHeader>
			{subtitle ? <IonCardSubtitle>{subtitle}</IonCardSubtitle> : null}
			<IonCardTitle>{title}</IonCardTitle>
		</IonCardHeader>
		<IonCardContent>
			{children}
		</IonCardContent>
	</IonCard>
)
