import React from "react";
import { IonHeader, IonToolbar, IonIcon, IonTitle, IonChip, IonAvatar, IonLabel } from "@ionic/react";
import { arrowDropdown } from "ionicons/icons";

interface Props {
	text?: string;
	imageUrl?:string;
	changeModalState: () => void;
}
export const DialogHeader = ({ text, imageUrl, changeModalState }: Props) => (
	<IonHeader translucent>
		<IonToolbar>
			{text && !imageUrl ? <IonTitle style={{ textTransform: 'capitalize' }}>{text}</IonTitle> : null}
			{imageUrl && text ?
			<IonChip>
				<IonAvatar>
					<img alt='avartar' src={imageUrl} />
				</IonAvatar>
				<IonLabel style={{ textTransform: 'capitalize' }}>{text}</IonLabel>
			</IonChip>
			: null}
			<IonIcon onClick={() => changeModalState()} icon={arrowDropdown} size="large" slot="end" className="close-icon" />
		</IonToolbar>
	</IonHeader>
)
