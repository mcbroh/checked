import React from "react";
import { IonCardContent, IonCard, IonIcon } from "@ionic/react";
import { checkmark } from "ionicons/icons";
import { AppButton } from ".";
import { languageService } from "../service";

interface Props {
	message: string;
	close: () => void;
	type?: 'success' | 'warning' | 'action_required'
}
export const MessageBoard = ({ message, close }: Props) => (
	<IonCard color="light" className="message-board">
		<div>
			<IonIcon size="large" icon={checkmark} />
		</div>
		<IonCardContent>
			{message}
			<AppButton text={languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'close')} onClick={() => close()} />
		</IonCardContent>
	</IonCard>
)
