import React from "react";
import { IonHeader, IonToolbar, IonTitle, IonButtons, IonBackButton, IonMenuButton } from "@ionic/react";

interface Props {
    history?: any;
    text?: string;
    back?: boolean;
    translucent?: boolean;
}
export const Header = ({ text, translucent, back, history }: Props) => {

    const renderButton = () => back ? <IonBackButton defaultHref="/" /> : <IonMenuButton />;

    return (
        <IonHeader translucent={translucent || false}>
            <IonToolbar>
                <IonButtons slot="start">
                    {renderButton()}
                </IonButtons>
                {text ?
                    <IonTitle style={{ textTransform: 'capitalize' }}>{text}</IonTitle>
                    :
                    <IonTitle className='app-name'>Jollo</IonTitle>
                }
            </IonToolbar>
        </IonHeader>
    );
}
