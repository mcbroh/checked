import React from 'react';
import { IonToast } from '@ionic/react';

type Props = {
	display: boolean;
	message: string | undefined;
	color?: 'success' | 'warning' | 'danger' | undefined;
	toastHidden: () => void;
}

export const Toast: React.FC<Props> = ({ display, message, toastHidden, color }: Props) => {
	return (
		<IonToast
			isOpen={display}
			message={message}
			color={color || 'danger'}
			onDidDismiss={() => toastHidden()}
			buttons={[
				{
					text: 'Done',
					role: 'cancel',
					handler: () => { }
				}
			]}
			duration={5000}
		/>
	);
};
