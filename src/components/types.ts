export type PickerProps = {
	name: string;
	isOpen: boolean;
	options: {
		text: string;
		value: any;
    }[];
    onCancel: () => void;
	onSelect: (inputName: string, event: any) => void;
}
