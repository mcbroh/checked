import React from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import {
    IonContent,
    IonHeader,
    IonIcon,
    IonItem,
    IonLabel,
    IonList,
    IonMenu,
    IonMenuToggle,
    IonToolbar
} from '@ionic/react';

import { AppPage } from '../declarations';
import { Logo } from './Logo';

interface MenuProps extends RouteComponentProps {
    appPages: AppPage[];
    isLoggedIn: boolean;
}


const SideMenu: React.FC<MenuProps> = ({ appPages, isLoggedIn }) => {

    const levels = {
        SUPER_ADMIN: 'superAdmin',
        SUB_SUPER_ADMIN: 'subSuperAdmin',
        ADMIN: 'admin',
        SUB_ADMIN: 'subAdmin',
        ORDINARY_USER: 'ordinaryUser'
    };
    const currentUser = JSON.parse(localStorage.getItem('USER') as any);

    function isRightPrivilege(state: boolean): boolean {
        if (
            currentUser &&
            (currentUser.privilege === levels.ADMIN ||
                currentUser.privilege === levels.SUB_ADMIN ||
                currentUser.privilege === levels.SUPER_ADMIN ||
                currentUser.privilege === levels.SUB_SUPER_ADMIN)
            && state === isLoggedIn
        ) {
            return true
        }
        return false;
    }

    return (
        <IonMenu contentId="main" type="overlay">
            <IonHeader>
                <IonToolbar>
                    <Logo />
                </IonToolbar>
            </IonHeader>
            <IonContent className="menu">
                <IonList>
                    {appPages.filter(page => page.loggedIn === isLoggedIn || page.loggedIn === null || isRightPrivilege(page.loggedIn)).map((appPage, index) => {
                        return (
                            <IonMenuToggle key={index} autoHide={false}>
                                <IonItem routerLink={appPage.path} routerDirection="none">
                                    <IonIcon slot="start" icon={appPage.icon} />
                                    <IonLabel>{appPage.title}</IonLabel>
                                </IonItem>
                            </IonMenuToggle>
                        );
                    })}
                </IonList>
                <div>Icons made by <a href="https://www.flaticon.com/authors/eucalyp" target="_blank" rel="noopener noreferrer" title="Eucalyp">Eucalyp</a> from <a href="https://www.flaticon.com/" target="_blank" rel="noopener noreferrer" title="Flaticon">www.flaticon.com</a></div>
            </IonContent>
        </IonMenu>
    );
}

export default withRouter(SideMenu);
