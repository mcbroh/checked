import React from 'react';
import { IonContent, IonSkeletonText } from '@ionic/react';

import { Logo } from '.';

const Splash: React.FC = () => {

    return (
        <IonContent>
            <section className="splash">
                <div />
                <Logo />
                <p>
                    <IonSkeletonText animated style={{ width: '100%' }}/>
                </p>
            </section>
        </IonContent>
    );
}

export default Splash;
