import React, { useEffect, useRef, useState } from 'react';
import {
    IonSlides, IonSlide, IonContent, IonItem, IonAvatar,
    IonSkeletonText, IonLabel, IonList, IonListHeader,
    IonSelect, IonSelectOption, IonChip
} from '@ionic/react';

import icon from '../assets/img/home.svg';
import slide2 from '../assets/img/slide-2.png';
import slide3 from '../assets/img/slide-3.png';
import { languageService } from '../service';
import { AppButton } from './AppButton';
import { useLocalStorage, usePrevious } from '../hooks';
import CheckCircle from '@material-ui/icons/CheckCircle';
import CancelOutlined from '@material-ui/icons/CancelOutlined';
import ArrowBack from '@material-ui/icons/ArrowBack';
import ArrowForward from '@material-ui/icons/ArrowForward';
import { getCountryRegions } from '../helpers/getCountryRegions';
import { createMarkup } from '../helpers';

interface Props {
	language: string;
	countriesList: any;
    updateLanguage: (value: string) => void;
}

const InitialState = {
    city: null,
    country: null,
	isAgent: false,
    displayImages: false,
}

export default function Slide({ language, updateLanguage, countriesList }: Props) {

    const swiper = useRef<any>(null);
    const [activeSlide, setActiveSlide] = useState(0);
    const [state, setState] = useLocalStorage('USER_PRESET', InitialState);
    const prevCount = usePrevious(state);

    useEffect(() => {
        swiper.current = document.getElementById('swiper') as any;
        swiper.current.options = {
            initialSlide: 0,
            speed: 400,
            allowTouchMove: false,
        };
    }, []);

    useEffect(() => {
        if (prevCount && (prevCount.country !== state.country)) {
            setState({ ...state, city: getCountryRegions(state.country, countriesList)[0].name });
        }
    }, [prevCount, state, setState, countriesList]);

    function slideNext() {
        swiper.current.slideNext();
        setActiveSlide(activeSlide + 1);
    }

    function slideBack() {
        swiper.current.slidePrev();
        setActiveSlide(activeSlide - 1);
    }

    function onCountryChange(event: CustomEvent) {
        event.preventDefault();
        const country = event.detail.value
        updateState({ country: country });
    }

    function updateState(params: object) {
        setState({ ...state, ...params })
    }

    function showButtons() {
        const activeSlideIndex = activeSlide;
        return <section className='btn-container'>
            {activeSlideIndex !== 0 && <AppButton mdButton disabled={!state.country || !state.city} onClick={() => slideBack()} children={<ArrowBack />} />}
            {activeSlideIndex !== 3 && <AppButton mdButton disabled={!state.country || !state.city} onClick={() => slideNext()} children={<ArrowForward />} />}
            {activeSlideIndex === 3 && <AppButton mdButton disabled={!state.country || !state.city} onClick={() => exist()} children={<CheckCircle />} />}
        </section>
    }

    function exist() {
        window.location.reload();
    }

    function onLanguage(value: any) {
        localStorage.setItem('USER_LANGUAGE', value);
        updateLanguage(value);
    }

    function getSlideText(param: string): string {
        return languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'SlidesText.' + param);
    }

    return (
        <IonContent className="slides">
            <IonSlides pager={false} id='swiper'>
                <IonSlide className="slide1">
                    <h3 dangerouslySetInnerHTML={createMarkup(getSlideText('slide1.header'))} />
                    <img src={icon} alt='slide 1' />
                    <IonList lines='full'>
                        <IonListHeader>{languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'locationPreset')}</IonListHeader>
                        <IonItem>
                            <IonLabel>{languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'language')}</IonLabel>
                            <IonSelect
                                value={language} interface="popover"
                                onIonChange={(e) => onLanguage(e.detail.value)}>
                                <IonSelectOption value="french">Français</IonSelectOption>
                                <IonSelectOption value="english">English</IonSelectOption>
                            </IonSelect>
                        </IonItem>

                        <IonItem>
                            <IonLabel>{languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'country')}</IonLabel>
                            <IonSelect
                                value={state.country}
                                onIonChange={(e) => onCountryChange(e)}
                                cancelText={languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'cancel')}>
                                {countriesList.map((country: any) => (
                                    <IonSelectOption key={country.code} value={country.code}>{country.name}</IonSelectOption>
                                ))}
                            </IonSelect>
                        </IonItem>

                        <IonItem>
                            <IonLabel>{languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'city')}</IonLabel>
                            <IonSelect
                                value={state.city}
                                onIonChange={(e) => updateState({ city: e.detail.value })}
                                cancelText={languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'cancel')}>
                                {getCountryRegions(state.country, countriesList).map((region: any) => (
                                    <IonSelectOption key={region.name} value={region.name}>{region.name}</IonSelectOption>
                                ))}
                            </IonSelect>
                        </IonItem>
                    </IonList>
                    {showButtons()}

                    <span>
                        {getSlideText('slide1.paragraph1') && <p dangerouslySetInnerHTML={createMarkup(getSlideText('slide1.paragraph1'))} />}
                        {getSlideText('slide1.paragraph2') && <p dangerouslySetInnerHTML={createMarkup(getSlideText('slide1.paragraph2'))} />}
                        {getSlideText('slide1.paragraph3') && <p dangerouslySetInnerHTML={createMarkup(getSlideText('slide1.paragraph3'))} />}
                    </span>
                </IonSlide>

                <IonSlide className="slide2">
                    <h3 dangerouslySetInnerHTML={createMarkup(getSlideText('slide2.header'))} />
                    <div className='ion-items'>
                        <IonChip onClick={() => updateState({ displayImages: true })}>
                            <IonLabel>{languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'picturesOption')}</IonLabel>
                            {state.displayImages ? <CheckCircle /> : <CancelOutlined />}
                        </IonChip>
                        {[1, 2,].map(value => (
                            <IonItem key={value} lines='full'>
                                <IonAvatar slot="start">
                                    <IonSkeletonText />
                                </IonAvatar>
                                <IonLabel>
                                    <h3>
                                        <IonSkeletonText style={{ width: '70%' }} />
                                    </h3>
                                    <p>
                                        <IonSkeletonText style={{ width: '100%' }} />
                                    </p>
                                </IonLabel>
                            </IonItem>
                        ))}
                    </div>

                    <div className='ion-items'>
                        <IonChip onClick={() => updateState({ displayImages: false })}>
                            <IonLabel>{languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'textOnlyOption')}</IonLabel>
                            {state.displayImages ? <CancelOutlined /> : <CheckCircle />}
                        </IonChip>
                        {[1, 2,].map(value => (
                            <IonItem key={value} lines='full'>
                                <IonLabel>
                                    <h3>
                                        <IonSkeletonText style={{ width: '70%' }} />
                                    </h3>
                                    <p>
                                        <IonSkeletonText style={{ width: '100%' }} />
                                    </p>
                                </IonLabel>
                            </IonItem>
                        ))}
                    </div>
                    {showButtons()}
                    <span>
                        {getSlideText('slide1.paragraph1') && <p dangerouslySetInnerHTML={createMarkup(getSlideText('slide1.paragraph1'))} />}
                        {getSlideText('slide1.paragraph2') && <p dangerouslySetInnerHTML={createMarkup(getSlideText('slide1.paragraph2'))} />}
                        {getSlideText('slide1.paragraph3') && <p dangerouslySetInnerHTML={createMarkup(getSlideText('slide1.paragraph3'))} />}
                    </span>
                </IonSlide>
                <IonSlide className="slide3">
                    <h3 dangerouslySetInnerHTML={createMarkup(getSlideText('slide3.header'))} />
                    <img src={slide2} alt='slide 1' />
                    <div className='ion-items agent-or-renter'>
                        <IonChip onClick={() => updateState({ isAgent: true })}>
                            <IonLabel>{languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'iWantToAdvertise')}</IonLabel>
                            {state.isAgent ? <CheckCircle /> : <CancelOutlined />}
                        </IonChip>
                        <IonItem lines='full'>
                            <IonLabel>{languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'reachMoreClients')}</IonLabel>
                        </IonItem>
                    </div>
                    <div className='ion-items agent-or-renter'>
                        <IonChip onClick={() => updateState({ isAgent: false })}>
                            <IonLabel>{languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'iAmSearchingProperties')}</IonLabel>
                            {state.isAgent ? <CancelOutlined /> : <CheckCircle />}
                        </IonChip>
                        <IonItem lines='full'>
                            <IonLabel>{languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'getMorOffers')}</IonLabel>
                        </IonItem>
                    </div>
                    {showButtons()}

                    <span>
                        {getSlideText('slide1.paragraph1') && <p dangerouslySetInnerHTML={createMarkup(getSlideText('slide1.paragraph1'))} />}
                        {getSlideText('slide1.paragraph2') && <p dangerouslySetInnerHTML={createMarkup(getSlideText('slide1.paragraph2'))} />}
                        {getSlideText('slide1.paragraph3') && <p dangerouslySetInnerHTML={createMarkup(getSlideText('slide1.paragraph3'))} />}
                    </span>
                </IonSlide>
                <IonSlide className="slide4">
                    <h3 dangerouslySetInnerHTML={createMarkup(getSlideText('slide3.header'))} />
                    <img src={slide3} alt='slide 1' />
                    {showButtons()}
                    <span>
                        {getSlideText('slide1.paragraph1') && <p dangerouslySetInnerHTML={createMarkup(getSlideText('slide1.paragraph1'))} />}
                        {getSlideText('slide1.paragraph2') && <p dangerouslySetInnerHTML={createMarkup(getSlideText('slide1.paragraph2'))} />}
                        {getSlideText('slide1.paragraph3') && <p dangerouslySetInnerHTML={createMarkup(getSlideText('slide1.paragraph3'))} />}
                    </span>
                </IonSlide>
            </IonSlides>
        </IonContent>
    );
};


