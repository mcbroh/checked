import React from 'react';
import logo from '../assets/img/home.svg';

export const Logo = () => (
    <div className="logo">
        <img className="logo" src={logo} alt='logo' />
        <p className='logo app-name'>Jollo</p>
    </div>
)
