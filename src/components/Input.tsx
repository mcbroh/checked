import React from 'react';
import { IonItem, IonLabel, IonInput, IonText, IonTextarea } from '@ionic/react';
import makeClassName from 'classnames';

import { validateField } from '../helpers';

interface Props {
    value: any;
    label: string;
    disabled?: boolean;
    checkValidity?: boolean;
    rule?: string;
    rows?: number
    placeholder?: string;
    textarea?: boolean
    readonly?: boolean;
    minlength?: number;
    maxlength?: number;
    ontouch?: () => void;
    onChange: (value: any, validity: boolean) => void;
    type?: "number" | "time" | "text" | "tel" | "url" | "email" | "search" | "date" | "password" | undefined;
}

export function Input({ value, disabled, label, onChange, type, rule, minlength, maxlength, ontouch, placeholder, rows, readonly, textarea }: Props) {

    const [formValidity, setFormValidity] = React.useState({
        inValid: false,
        showValidity: false,
        message: '',
    });

    function handleChange(value: string) {
        const valid = validateField(rule, minlength, maxlength, value)
        if (formValidity.inValid) {
            setFormValidity({ showValidity: valid.hasError, inValid: valid.hasError, message: valid.errorMsg });
        } else {
            setFormValidity({ ...formValidity, inValid: valid.hasError });
        }
        onChange(value, valid.hasError);
    };

    function handleTouch() {
        ontouch && ontouch();
    }

    function handleBlur(currentValue = value) {
        const valid = validateField(rule, minlength, maxlength, currentValue);
        setFormValidity({ showValidity: valid.hasError, inValid: valid.hasError, message: valid.errorMsg });
    };

    const Input = textarea ? IonTextarea : IonInput;

    return (
        <>
            <IonItem
                lines="full"
                className={
                    makeClassName([
                        'filled',
                        {
                            'has-error': formValidity.inValid && formValidity.showValidity,
                        }
                    ])
                }
            >
                <IonLabel position="floating">{label}</IonLabel>
                <Input
                    type={type}
                    value={value}
                    rows={rows || 2}
                    placeholder={placeholder || undefined}
                    disabled={disabled}
                    readonly={readonly || false}
                    minlength={minlength || undefined}
                    maxlength={maxlength || undefined}
                    onTouchEnd={() => handleTouch()}
                    onIonBlur={() => handleBlur()}
                    onIonChange={(event: any) => handleChange(event.target.value)}
                ></Input>
            </IonItem>
            <IonText color="danger">
                <p className="input-error-message">{formValidity.inValid && formValidity.showValidity ? formValidity.message : ''}</p>
            </IonText>
        </>
    )
}
