import React, { CSSProperties } from 'react';
import nothing from '../assets/img/nothing.png';
import { languageService } from '../service';

const styles = {
    h3: {
        fontWeight: 100,
        textTransform: 'uppercase',
    } as CSSProperties,
    container: {
        display: 'flex',
        flexDirection: "column",
        width: '100%',
        height: '100%',
        justifyContent: 'space-between',
        alignItems: 'center',
    } as CSSProperties,
    iconAds: {
        width: '100%',
        fontSize: '50%',
        textAlign: 'center',
        marginBottom: '10px',
    } as CSSProperties,
};

export const Empty = () => (
    <div style={styles.container}>
        <h3 style={styles.h3}>{languageService.getText(localStorage.getItem('USER_LANGUAGE'), 'noProperties')}</h3>
        <div className="image">
            <img src={nothing} alt='Nothing to show' />
        </div>
        <div style={styles.iconAds}>
            Icons made by <a href="https://www.flaticon.com/authors/eucalyp" target="_blank" rel="noopener noreferrer" title="Eucalyp">Eucalyp</a> from <a href="https://www.flaticon.com/" target="_blank" rel="noopener noreferrer" title="Flaticon"> www.flaticon.com</a>
        </div>
    </div>
);
