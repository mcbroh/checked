import React, { ReactElement } from 'react';

import { IonButton, IonIcon, IonRippleEffect } from "@ionic/react";
import Button from '@material-ui/core/Button';

interface Props {
    text?: string;
    children?: ReactElement;
    showIcon?: boolean;
    iconName?: string;
    disabled?: boolean;
    expand?: 'block' | 'full';
    iconSlot?: 'start' | 'end';
    mdButton?: boolean | undefined;
    onClick?: (event: any) => void;
    color?: 'danger' | 'dark' | 'light' | 'warning';
}

export const AppButton = ({ color, expand, showIcon, iconSlot, iconName, text, disabled, onClick, mdButton, children }: Props) => {

    if (mdButton) {
        return (
            <Button
                color="primary"
                disabled={disabled}
                variant="outlined"
                onClick={onClick}>
                {text || children}
            </Button>
        );
    }

    return (
        <IonButton
            className='button'
            color={color || 'dark'}
            expand={expand || "block"}
            disabled={disabled}
            mode='ios'
            onClick={onClick}
        >
            {showIcon ? <IonIcon slot={iconSlot || "start"} name={iconName || "star"} /> : null}
            {text && text.toUpperCase()}
            <IonRippleEffect type="unbounded"></IonRippleEffect>
        </IonButton>
    )

}
