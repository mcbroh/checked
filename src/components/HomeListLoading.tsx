import React from 'react';
import {
    IonContent,
    IonSkeletonText,
    IonAvatar,
    IonGrid,
    IonRow,
    IonCol
} from '@ionic/react';

export function HomeListLoading() {
    return (
        <IonContent className='home-loading'>
            <IonGrid>
                <IonRow>
                    <IonAvatar className='header-img'>
                        <IonSkeletonText animated />
                    </IonAvatar>
                    {
                        [1, 2, 3, 4, 5, 6, 7, 8].map(value => (
                            <IonCol key={value} size='12' sizeMd='6'>
                                <IonAvatar className='body-img'>
                                    <IonSkeletonText animated />
                                </IonAvatar>
                            </IonCol>
                        ))
                    }
                </IonRow>
            </IonGrid>
        </IonContent>
    );
};
