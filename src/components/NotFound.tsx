import React from 'react';
import { AppButton } from '.';

export const NotFound = (props: any) => {
    function goBack() {
        window.location.href = '/properties';
    }

    return (
        <div className="notfound">
            <div className="container">
                <div className="notfound-404">
                    <h1>Oops!</h1>
                    <h2>404 - The Page can't be found</h2>
                </div>
                <AppButton text='go back' onClick={() => goBack()}/>
            </div>
        </div>
    );
}
