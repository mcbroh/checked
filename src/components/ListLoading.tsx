import React from 'react';
import {
	IonContent,
	IonItem,
	IonLabel,
	IonSkeletonText,
	IonListHeader,
	IonAvatar,
	IonList
} from '@ionic/react';

export function ListLoading() {
	return (
		<IonContent>
			<IonList>
				<IonListHeader>
					<IonSkeletonText animated style={{ width: '20%' }} />
				</IonListHeader>
				{
					[1,2,3,4,5,6,7,8,9].map(value => (
						<IonItem key={value}>
							<IonAvatar slot="start">
								<IonSkeletonText animated />
							</IonAvatar>
							<IonLabel>
								<h3>
									<IonSkeletonText animated style={{ width: '50%' }} />
								</h3>
								<p>
									<IonSkeletonText animated style={{ width: '80%' }} />
								</p>
							</IonLabel>
						</IonItem>
					))
				}
			</IonList>
		</IonContent>
	);
};
