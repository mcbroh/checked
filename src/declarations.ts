export interface AppPage {
    title: string;
    path?: string;
    icon?: any;
    privilege?: boolean;
    loggedIn: boolean | null;
}
