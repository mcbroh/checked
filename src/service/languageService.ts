import { fr } from "./languages/fr";
import { eng } from "./languages/eng";

const languagePack = {
    french: fr,
    english: eng,
}

class LanguageService {
    public getText(language: any, text: string): string {
        return this.getNestedData((languagePack as any)[language], text);
    }

    getNestedData(obj: any, path: string) {
        var index = function(obj: any, i: any) { return obj && obj[i]; };
        return path.split('.').reduce(index, obj);
      }
}

export const languageService = new LanguageService();
