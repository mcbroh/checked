import { backendService } from './backendService';

export type TransactionResponse = {
    customerDetails: {
        image: string;
        fullName: string;
        email: string;
        username: string;
    }
    status: number;
    clientId: string;
    paymentDate: string;
    paymentStart: string;
    transactionId: string;

    updated: string; // "2019 10 27, 07:14"
    notificationDate: string; // "2020-01-01"
    isPaid: boolean;
    _id: string;
    customerId: string;
    companyId: string;
    amountDue: string;
    recurrent: number // 1
    monthlyPay: string;
    contractType: string; // "longTerm"
    interestRate: string;
    totalDue: string;
    paymentInterval: string; // "1"
    lengthOfRepayment: string; // "60"
    totalPlusInterest: string; // "2000"
}

type TransactionDetails = {
    status: {
        1: string;
        2: string;
        3: string;
    };
    transactions: TransactionResponse;
}

class TransactionService {

    public getTransactions() {
        return backendService.get('transactions');
    }
    public updateTransactions(route: string, data: any) {
        return backendService.patch(route, data);
    }
}

export const transactionService = new TransactionService();
