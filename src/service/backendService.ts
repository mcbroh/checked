const axios = require('axios');

const baseUrl = process.env.NODE_ENV === 'production' ? 'https://pushfram.herokuapp.com/api/' : 'http://localhost:3002/api/';

class BackendService {

    public async get(uri: string) {
        return await axios.get(baseUrl + uri, this.appendHeaders()).then(function (response: any) {
            return response.data;
        })
            .catch(function (error: any) {
                return {
                    success: false,
                    message: error.message,
                };
            });
    }

    public async post(uri: string, data: any) {
        return await axios.post(baseUrl + uri, data, this.appendHeaders())
            .then(function (response: any) {
                return response.data;
            })
            .catch(function (error: any) {
                return {
                    success: false,
                    message: error.message,
                };
            });
    }

    public async patch(uri: string, data: any) {
        return await axios.patch(baseUrl + uri, data, this.appendHeaders())
            .then(function (response: any) {
                return response.data;
            })
            .catch(function (error: any) {
                return {
                    success: false,
                    message: error.message,
                };
            });
    }

    private appendHeaders() {
        const token = localStorage.getItem('TOKEN');
        return {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': token
            }
        };
    }

    public async uploadImage(data: any, uri: string) {
        const formData = await this.createFormData(data);
        const token = localStorage.getItem('TOKEN');

        return await axios.post(baseUrl + uri, formData, {
            headers: {
                'Content-Type': 'multipart/form-data',
                'Authorization': token
            },
        }).then(function (response: any) {
            return response.data;
        }).catch(function (error: any) {
            return {
                success: false,
                message: error.message,
            };
        });

    }

    private async createFormData(data: any) {
        const formData = new FormData() as any;
        const retValArr = Object.keys(data);
        retValArr.forEach(val => {
            formData.append(val, data[val]);
        });

        return formData;
    }

}

export const backendService = new BackendService();
