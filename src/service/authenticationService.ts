import { backendService } from './backendService';

class AuthenticationService {

    public login(data: any) {
        return backendService.post('companies/signin', data);
    }
}

export const authenticationService = new AuthenticationService();
