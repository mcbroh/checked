const fs = require('fs');
const chai = require('chai');
const chaiHttp = require('chai-http');


//Assertion styles
chai.should();
chai.use(chaiHttp);
const host = 'http://192.168.1.16:3002';

const location = {
    country: 'NG',
    city: 'ibadan'
}
let user = { password: 'password' };
let companyUser = {};
let propertyId = null;
/*

POST	Create	201 (Created), 'Location' header with link to /customers/{id} containing new ID.	404 (Not Found), 409 (Conflict) if resource already exists..
GET	Read	200 (OK), list of customers. Use pagination, sorting and filtering to navigate big lists.	200 (OK), single customer. 404 (Not Found), if ID not found or invalid.
PUT	Update/Replace	405 (Method Not Allowed), unless you want to update/replace every resource in the entire collection.	200 (OK) or 204 (No Content). 404 (Not Found), if ID not found or invalid.
PATCH	Update/Modify	405 (Method Not Allowed), unless you want to modify the collection itself.	200 (OK) or 204 (No Content). 404 (Not Found), if ID not found or invalid.
DELETE	Delete	405 (Method Not Allowed), unless you want to delete the whole collection—not often desirable.
*/

describe('Customer API', () => {


    describe('TEST create company', () => {

        it('It should create a new company', (done) => {
            const company = {
                name: 'Chai test',
                phone: '0777777777',
                street: 'all street',
                streetNo: '999',
                companyId: '111111',
                companyName: 'chai-company',
                city: location.city,
                country: location.country,
                email: 'chai@chai.gmail',
            };
            chai.request(host)
                .post('/api/companies')
                .send(company)
                .end((err, response) => {
                    response.should.have.status(200);
                    response.body.should.be.a('object');
                    response.body.should.have.property('id');
                    response.body.should.have.property('name');
                    response.body.should.have.property('email');
                    response.body.should.have.property('username');
                    user.id = response.body.id;
                    user.username = response.body.username;
                    done();
                });
        });

        it('It should sign in company', (done) => {
            const loggedInUser = {
                username: user.username,
                password: user.password
            };
            chai.request(host)
                .get('/api/companies')
                .send(loggedInUser)
                .end((err, response) => {
                    response.should.have.status(200);
                    response.body.should.be.a('object');
                    response.body.user.should.be.a('object');
                    response.body.should.have.property('token');
                    user.token = response.body.token;
                    done();
                });
        });

        it('It should create a user ', (done) => {
            const company = {
                name: 'company user',
                username: 'myuser',
                email: 'myuser@company.gmail',
            };
            chai.request(host)
                .post('/api/companies/user')
                .set('authorization', user.token)
                .send(company)
                .end((err, response) => {
                    response.should.have.status(200);
                    response.body.should.be.a('object');
                    response.body.should.have.property('name');
                    response.body.should.have.property('email');
                    response.body.should.have.property('username');
                    companyUser.username = response.body.username;
                    done();
                });
        });

        it('It should get company users', (done) => {
            chai.request(host)
                .get('/api/companies/myUsers')
                .set('authorization', user.token)
                .end((err, response) => {
                    response.should.have.status(200);
                    response.body.users.should.be.a('array');
                    response.body.users[0].should.be.a('object');
                    done();
                });
        });

    });

    describe('Test /api/property route', () => {

        it('It should create a new property', (done) => {
            const file = 'data:image/png;base64,' + fs.readFileSync(__dirname + '/image/nothing.png', { encoding: 'base64' });
            const advert = {
                rent: '100',
                numOfRooms: '3',
                totalSpace: '170',
                location: 'hhh',
                introText: 'intro test',
                images: [{ name: 'chai', src: file }],
                city: location.city,
                advertType: 'rent',
                paymentOptions: 'monthly',
                generalDetails: 'none',
            };
            chai.request(host)
                .post('/api/property')
                .send(advert)
                .set('authorization', user.token)
                .end((err, response) => {
                    response.should.have.status(200);
                    response.body.should.be.a('object');
                    response.body.property.should.have.property('mainImage');
                    propertyId = response.body.property._id;
                    done();
                });
        });

        it('It should delete property', (done) => {
            chai.request(host)
                .delete('/api/property')
                .send({ id: propertyId })
                .set('authorization', user.token)
                .end((err, response) => {
                    response.should.have.status(200);
                    response.body.should.be.a('object');
                    response.body.property.should.have.property('_id');
                    response.body.should.have.property('success').eq(true);
                    done();
                });
        });

        it('It should create a new property', (done) => {
            const file = 'data:image/png;base64,' + fs.readFileSync(__dirname + '/image/nothing.png', { encoding: 'base64' });
            const advert = {
                rent: '100',
                numOfRooms: '3',
                totalSpace: '170',
                location: 'hhh',
                introText: 'intro test',
                images: [{ name: 'chai', src: file }],
                city: location.city,
                advertType: 'rent',
                paymentOptions: 'monthly',
                generalDetails: 'none',
            };
            chai.request(host)
                .post('/api/property')
                .send(advert)
                .set('authorization', user.token)
                .end((err, response) => {
                    response.should.have.status(200);
                    response.body.should.be.a('object');
                    response.body.property.should.have.property('mainImage');
                    propertyId = response.body.property._id;
                    done();
                });
        });

    });

    describe('Test delete company', () => {

        it('It should delete company', (done) => {
            chai.request(host)
                .delete('/api/companies')
                .set('authorization', user.token)
                .end((err, response) => {
                    response.should.have.status(200);
                    response.body.should.be.a('object');
                    response.body.should.have.property('success').eq(true);
                    response.body.should.have.property('count').eq(2);
                    done();
                });
        });

        it('It should return unauthorized company', (done) => {
            const loggedInUser = {
                username: user.username,
                password: user.password
            };
            chai.request(host)
                .get('/api/companies')
                .send(loggedInUser)
                .end((err, response) => {
                    response.should.have.status(401)
                    done();
                });
        });

        it('It should return unauthorized company user', (done) => {
            const loggedInUser = {
                username: companyUser.username,
                password: user.password
            };
            chai.request(host)
                .get('/api/companies')
                .send(loggedInUser)
                .end((err, response) => {
                    response.should.have.status(401)
                    done();
                });
        });

        it('Property should not exist', (done) => {
            chai.request(host)
                .get('/api/property/'+propertyId)
                .end((err, response) => {
                    response.body.should.have.property('success').eq(false);
                    done();
                });
        });
    })
})
