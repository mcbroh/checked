const mongoose = require('mongoose');

const Required = {
    type: String,
    required: true,
};

const propertySchema = mongoose.Schema({
    rent: Required,
    numOfRooms: Required,
    totalSpace: Required,
    location: Required,
    introText: Required,
    generalDetails: Required,
    mainImage: Required,
    city: Required,
    country: Required,
    currency: Required,
    paymentOptions: Required,
    advertType: Required,
    trueId: Required,
    images: [{
        key: Required,
        name: Required,
    }],
    advertiser: {
        phone: Required,
        name: Required,
    },
});


module.exports = mongoose.model('Property', propertySchema);
