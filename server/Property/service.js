const Property = require("./model");
const ImageService = require('../services/imageService');
const CountriesList =  require('../Config/countriesList');


module.exports = class PropertyService {

    errorMessage(error) {
        return { success: false, message: error };
    }

    response(data) {
        return { success: true, ...data }
    }

    createAd(user, data) {
        const { trueId, company: { phone, name, country } } = user;
        const imageService = new ImageService();
        const promises = [];
        const timeSubmitted = Date.now();
        const imageUrl = [];

        const advertiser = {
            phone: phone,
            name: name,
        };

        const advertLocation = {
            city: data.city,
            country: country,
            currency: CountriesList.Currency[country]
        };

        for (let index = 0; index < data.images.length; index++) {
            const file = data.images[index];
            const key = `${trueId}/${timeSubmitted}/${index}`;
            imageUrl.push({key, name: file.name});
            promises.push(imageService.setPropertyImages(key, file.src));
        };

        return Promise.all(promises).then(async (imgs) => {
            const mainImage = imageUrl[0].key;
            const images = imageUrl.slice(1, imageUrl.length);
            const property = new Property({ ...data, trueId, ...advertLocation, mainImage, images, advertiser });
            try {
                await property.save();
                return this.response({property});
            } catch (e) {
                return this.errorMessage(e);
            }
        }).catch(err => {
            return this.errorMessage(err);
        })
    }

    async getPropertyLists(country) {
        try {
            const properties = await Property.find({country: country}).select({
                'currency': 1,
                'rent': 1,
                'paymentOptions': 1,
                'advertType': 1,
                'numOfRooms': 1,
                'totalSpace': 1,
                'location': 1,
                'introText': 1,
                'mainImage': 1,
                'advertiser.name': 1
            })
            return this.response({url: process.env.S3_BUCKET_PROPERTY_URL, data: properties});
        } catch (error) {
            return this.errorMessage(error.message);
        }
    }

    async getSingleProperty(id) {
        try {
            const property = await Property.findById({ _id: id });
            if (property) {
                return this.response({url: process.env.S3_BUCKET_PROPERTY_URL, data: property});
            } else {
                return this.errorMessage('Property not found');
            }
        } catch (error) {
            return this.errorMessage(error.message);
        }
    }

    async getMyAds(user) {
        const { company: { companyId } } = user;
        try {
            const properties = await Property.find({ 'advertiser.companyId': companyId });
            return this.response({url: process.env.S3_BUCKET_PROPERTY_URL, data: properties});
        } catch (error) {
            return this.errorMessage(error.message);
        }
    }

    async deleteProperty(trueId, id) {
        try {
            const property = await Property.findOne({ _id: id, trueId: trueId });
            if (property) {
                const promises = [];
                const imageService = new ImageService();
                for (let index = 0; index < property.images.length; index++) {
                    const key = property.images[index];
                    promises.push(imageService.deletePropertyImage(key));
                };
                promises.push(imageService.deletePropertyImage(property.mainImage));

                return Promise.all(promises).then(async (res) => {
                    try {
                        property.remove();
                        return this.response({property})
                    } catch (e) {
                        return this.errorMessage(e);
                    }
                }).catch(err => {
                    return this.errorMessage(err);
                })

            } else {
                return this.errorMessage('Property does not exist');
            }
        } catch (error) {
            return this.errorMessage(error.message);
        }
    }

}
