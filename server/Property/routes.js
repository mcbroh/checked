const express = require('express');
const router = new express.Router();
const passport = require('passport');
const PropertyController = require('./controller');
const passportJWT = passport.authenticate('jwt', { session: false });

router.post('/', passportJWT, PropertyController.create);
router.delete('/', passportJWT, PropertyController.delete);
router.get('/mine', passportJWT, PropertyController.myAds);
router.get('/:id', PropertyController.getOne);
router.get('/:country/all', PropertyController.getAll);

module.exports = router;
