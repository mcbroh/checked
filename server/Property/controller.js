const Joi = require("@hapi/joi");
const PropertyService = require('./service');


module.exports = {

    create: async (req, res) => {

        const schema = {
            rent: Joi.required(),
            numOfRooms: Joi.required(),
            totalSpace: Joi.required(),
            location: Joi.required(),
            introText: Joi.required(),
            images: Joi.required(),
            city: Joi.required(),
            advertType: Joi.required(),
            paymentOptions: Joi.required(),
            generalDetails: Joi.required(),
        };

        try {
            await Joi.validate(req.body, schema);
        } catch (err) {
            return res.status(200).send({ success: false, message: err.details[0].message });
        }

        try {
            const property = new PropertyService();
            property.createAd(req.user, req.body).then(value => {
                res.send(value);
            });

        } catch (error) {
            console.log('error: ', error.message);
            res.status(500).send(error.message);
        }
    },

    // Read
    getAll: async (req, res) => {
        try {
            const property = new PropertyService();
            property.getPropertyLists(req.params.code).then(properties => {
                res.send(properties);
            });
        } catch (error) {
            console.log('error: ', error.message);
            res.status(500).send(error.message);
        }
    },

    getOne: async (req, res) => {
        try {
            const property = new PropertyService();
            property.getSingleProperty(req.params.id).then(property => {
                res.send(property);
            });
        } catch (error) {
            console.log('error: ', error.message);
            res.status(500).send(error.message);
        }
    },

    myAds: async (req, res) => {
        try {
            const property = new PropertyService();
            property.getMyAds(req.user).then(properties => {
                res.send(properties);
            });
        } catch (error) {
            console.log('error: ', error.message);
            res.status(500).send(error.message);
        }
    },

    // Update

    // Delete
    delete: async (req, res) => {
        try {
            const property = new PropertyService();
            property.deleteProperty(req.user.trueId, req.body.id).then(property => {
                res.send(property);
            });
        } catch (error) {
            console.log('error: ', error.message);
            res.status(500).send(error.message);
        }
    },

};
