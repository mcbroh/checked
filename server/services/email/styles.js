module.exports = `
    <style type="text/css">
        body {
            Margin:0;
            padding:0;
            width:100%;
            -ms-text-size-adjust:100%;
            -webkit-text-size-adjust:100%;
            font-family:'open sans', 'helvetica neue', helvetica, arial, sans-serif;
        }
        .es-wrapper {
            Margin:0;
            padding:0;
            width:100%;
            height:100%;
            border-spacing:0px;
            mso-table-lspace:0pt;
            mso-table-rspace:0pt;
            background-repeat:repeat;
            border-collapse:collapse;
            background-position:center top;
        }
        .es-wrapper-main {
            border: solid 1px #EEEEEE;
            background-color: #ffffff;
        }
        .es-header-body {
            mso-table-lspace:0pt;
            mso-table-rspace:0pt;
            border-collapse:collapse;
            border-spacing:0px;
            background-color: transparent;
            border-bottom: solid 1px #EEEEEE;
        }
        .logo-img {
            border-width:0;
            max-width:59px;
            height:auto;
            display:block;
            padding-right:20px
        }
        .app-name {
            Margin:0;
            color: #424242;
            font-size:36px;
            font-weight:bold;
            line-height:36px;
            font-style:normal;
            mso-line-height-rule:exactly;
            font-family:'open sans', 'helvetica neue', helvetica, arial, sans-serif;
        }
        .es-hb-1 {
            Margin:0;
            padding-top:35px;
            padding-bottom:35px;
            padding-left:35px;
            padding-right:35px;
        }
        .es.ft-1 {
            table-layout:fixed !important;
            width:100%;
            background-color:transparent;
            background-repeat:repeat;
            background-position:center top;
        }
        .footer-p {
            Margin:0;
            color:#777777;
            font-size:14px;
            line-height:21px;
            -ms-text-size-adjust:none;
            mso-line-height-rule:exactly;
            -webkit-text-size-adjust:none;
            font-family:'open sans', 'helvetica neue', helvetica, arial, sans-serif;
        }
        .address-p {
            Margin:0;
            color:#333333;
            font-size:14px;
            line-height:21px;
            -ms-text-size-adjust:none;
            mso-line-height-rule:exactly;
            -webkit-text-size-adjust:none;
            font-family:'open sans', 'helvetica neue', helvetica, arial, sans-serif;
        }
        .ft-1 {
            background-color:#EEEEEE;
            border-top:solid 1px #424242;
            color: #000000
        }
        #outlook a {
            padding:0;
        }
        .ExternalClass {
            width:100%;
        }
        .ExternalClass,
        .ExternalClass p,
        .ExternalClass span,
        .ExternalClass font,
        .ExternalClass td,
        .ExternalClass div {
            line-height:100%;
        }
        .es-button {
        mso-style-priority:100!important;
        text-decoration:none!important;
        }
        .button a[x-apple-data-detectors] {
        color:inherit!important;
        text-decoration:none!important;
        font-size:inherit!important;
        font-family:inherit!important;
        font-weight:inherit!important;
        line-height:inherit!important;
        }
        .es-desk-hidden {
        display:none;
        float:left;
        overflow:hidden;
        width:0;
        max-height:0;
        line-height:0;
        mso-hide:all;
        }

        tr {
            border-collapse:collapse;
        }
        .pt15 {
            padding-top:15px !important;
        }
        .pb10 {
            padding-bottom:10px !important;
        }
        .pb20 {
            padding-bottom:20px !important;
        }
        .pt20 {
            padding-top:20px !important;
        }
        .pm0 {
            padding:0;
            Margin:0;
        }
        .button a {
            mso-style-priority:100 !important;
            text-decoration:none;
            -webkit-text-size-adjust:none;
            -ms-text-size-adjust:none;
            mso-line-height-rule:exactly;
            font-size:18px;
            color:#FFFFFF;
            border-style:solid;
            border-color:#ED8E20;
            border-width:15px 30px;
            display:inline-block;
            background:#ED8E20 none repeat scroll 0% 0%;
            border-radius:5px;
            font-weight:normal;
            font-style:normal;
            line-height:22px;
            width:auto;
            text-align:center;
            font-family:'open sans', 'helvetica neue', helvetica, arial, sans-serif;
        }
        h2 {
            Margin:0;
            line-height:29px;
            mso-line-height-rule:exactly;
            font-size:24px;
            font-style:normal;
            font-weight:bold;
            color:#333333;
            font-family:'open sans', 'helvetica neue', helvetica, arial, sans-serif;
        }
        h3 {
            Margin:0;
            line-height:22px;
            mso-line-height-rule:exactly;
            font-size:18px;
            font-style:normal;
            font-weight:bold;
            color:#333333;
            font-family:'open sans', 'helvetica neue', helvetica, arial, sans-serif;
        }
        p {
            Margin:0;
            -webkit-text-size-adjust:none;
            -ms-text-size-adjust:none;
            mso-line-height-rule:exactly;
            font-size:16px;
            line-height:24px;color:#777777;
            font-family:'open sans', 'helvetica neue', helvetica, arial, sans-serif;
        }
        .es-button-border {
            border-style:solid;
            border-color:transparent;
            border-width:0px;
            display:inline-block;
            border-radius:5px;
            width:auto;
            background:#ED8E20 none repeat scroll 0% 0%;
        }
        .es-content {
            mso-table-lspace:0pt;
            mso-table-rspace:0pt;
            border-collapse:collapse;
            border-spacing:0px;
            table-layout:fixed !important;
            width:100%;
        }
        table {
            mso-table-lspace:0pt;
            mso-table-rspace:0pt;
            border-collapse:collapse;
            border-spacing:0px;
        }
        .es-content-body {
            mso-table-lspace:0pt;
            mso-table-rspace:0pt;
            border-collapse:collapse;
            border-spacing:0px;
            background-color:#FFFFFF;
        }
        .message-container {
            padding-left:35px;
            padding-right:35px;
            padding-top:40px;
        }
        .ts-sf-01 {
            Margin:0;
            padding-top:30px;
            padding-bottom:35px;
            padding-left:35px;
            padding-right:35px;
        }
        .td-sf-02 {
            padding-bottom:15px;
            padding-top:30px;
        }
        .td-sf-x1 {
            padding:0;
            Margin:0px;
            height:1px;
            width:100%;
            margin:0px;
            border-bottom:3px solid #EEEEEE;
            background:rgba(0, 0, 0, 0) none repeat scroll 0% 0%;
        }
        @media only screen and (max-width:600px) {
            p, ul li, ol li, a {
                font-size:16px!important;
                line-height:150%!important
            }
            h1 {
                font-size:32px!important; text-align:center;
                line-height:120%!important
            }
            h2 {
                font-size:26px!important;
                text-align:center;
                line-height:120%!important
            }
            h3 {
                font-size:20px!important;
                text-align:center;
                line-height:120%!important
            }
            h1 a {
                font-size:32px!important
            }
            h2 a {
                font-size:26px!important
            }
            h3 a {
                font-size:20px!important
            }
            .es-menu td a {
                font-size:16px!important
            }
            .es-header-body p,
            .es-header-body ul li,
            .es-header-body ol li,
            .es-header-body a {
                font-size:16px!important
            }
            .es-footer-body p,
            .es-footer-body ul li,
            .es-footer-body ol li,
            .es-footer-body a {
                font-size:16px!important
            }
            .es-infoblock p,
            .es-infoblock ul li,
            .es-infoblock ol li,
            .es-infoblock a {
                font-size:12px!important
            }
            *[class="gmail-fix"] {
                display:none!important
            }
            .es-m-txt-c,
            .es-m-txt-c h1,
            .es-m-txt-c h2,
            .es-m-txt-c h3 {
                text-align:center!important
            }
            .es-m-txt-r,
            .es-m-txt-r h1,
            .es-m-txt-r h2,
            .es-m-txt-r h3 {
                text-align:right!important
            }
            .es-m-txt-l,
            .es-m-txt-l h1,
            .es-m-txt-l h2,
            .es-m-txt-l h3 { text-align:left!important }
            .es-m-txt-r img,
            .es-m-txt-c img,
            .es-m-txt-l img {
                display:inline!important
            }
            .es-button-border {
                display:inline-block!important
            }
            a .es-button {
                font-size:16px!important;
                display:inline-block!important;
                border-width:15px 30px 15px 30px!important
            }
            .es-btn-fw {
                border-width:10px 0px!important;
                text-align:center!important
            }
            .es-adaptive table,
            .es-btn-fw,
            .es-btn-fw-brdr,
            .es-left,
            .es-right {
                width:100%!important
            }
            .es-content table,
            .es-header table,
            .es-footer table,
            .es-content,
            .es-footer,
            .es-header {
                width:100%!important;
                max-width:600px!important
            }
            .es-adapt-td {
                display:block!important;
                width:100%!important
            }
            .adapt-img {
                width:100%!important;
                height:auto!important
            }
            .es-m-p0 {
                padding:0px!important
            }
            .es-m-p0r {
                padding-right:0px!important
            }
            .es-m-p0l {
                padding-left:0px!important
            }
            .es-m-p0t {
                padding-top:0px!important
            }
            .es-m-p0b {
                padding-bottom:0!important
            }
            .es-m-p20b {
                padding-bottom:20px!important
            }
            .es-mobile-hidden, .es-hidden {
                display:none!important
            }
            .es-desk-hidden {
                display:table-row!important;
                width:auto!important;
                overflow:visible!important;
                float:none!important;
                max-height:inherit!important;
                line-height:inherit!important
            }
            .es-desk-menu-hidden {
                display:table-cell!important
            }
            table.es-table-not-adapt, .esd-block-html table {
                width:auto!important
            }
            table.es-social {
                display:inline-block!important
            }
            table.es-social td {
                display:inline-block!important
            }
        }
    </style>
`;

