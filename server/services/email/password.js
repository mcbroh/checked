module.exports = {
    getPasswordResetURL: (user, token) => {
        const route = process.env.NODE_ENV === 'development' ? 'http://localhost:8100' : process.env.WEBSITE_ADD;
        return `${route}/password/reset/${user._id}/${token}`;
    },

    resetPasswordTemplate: (url) => `<tr>
    <td align="left" class="td-sf-01">
        <table width="100%" cellspacing="0" cellpadding="0">
            <tr>
                <td width="530" valign="top" align="center" class="pm0">
                    <table width="100%" cellspacing="0" cellpadding="0">
                        <tr>
                            <td align="center" class="pm0 pt15">
                                <p>You can use the link bellow to reset your password:</p>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" class="pm0 pt15">
                            <a href=${url}>Follow Link</a>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" class="pm0 td-sf-02">
                            <p>If you don’t use this link within 1 hour, it will expire.</p>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </td>
</tr>`,
}
