const nodemailer = require("nodemailer");


module.exports = {
    createTransport:  nodemailer.createTransport({
        host: 'smtp.zoho.eu',
        Port: 465,
        secure: true,
        auth: {
            user: process.env.MAIL_ADD,
            pass: process.env.MAIL_PASS
        }
    })
}
