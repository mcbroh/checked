const mailBody = require('./header-footer');
const styles = require('./styles');
module.exports = {
    mainEmailTemplate: (name, state, introductionMessage = '') => `
        ${styles}
        ${mailBody.header}
        <table class="es-content" cellspacing="0" cellpadding="0" align="center">
            <tr>
                <td align="center" class="pm0">
                    <table class="es-content-body" width="600" cellspacing="0" cellpadding="0" bgcolor="#ffffff" align="center">
                    <tr>
                        <td align="left" class="pm0 message-container">
                            <table width="100%" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td width="530" valign="top" align="center" class="pm0">
                                        <table width="100%" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td class="es-m-txt-l" align="left" class="pm0 pt15">
                                                    <p><strong>Salut</strong> ${name}<strong>!</strong></p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" class="pm0 pt15 pb10">
                                                    <p>${introductionMessage}</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="pm0 pt20 pb20">
                                                    <table width="100%" height="100%" cellspacing="0" cellpadding="0" border="0">
                                                        <tr>
                                                            <td class="td-sf-x1"></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                        ${state}
                    </table>
                </td>
            </tr>
        </table>
        ${mailBody.footer}
 `,
}
