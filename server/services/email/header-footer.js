module.exports = {
    header: `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html>
    <head>
      <meta charset="UTF-8">
      <meta content="width=device-width, initial-scale=1" name="viewport">
      <meta name="x-apple-disable-message-reformatting">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta content="telephone=no" name="format-detection">
      <title>New email template 2019-07-30</title>
      <!--[if (mso 16)]>    <style type="text/css">    a {text-decoration: none;}    </style>    <![endif]-->
      <!--[if gte mso 9]><style>sup { font-size: 100% !important; }</style><![endif]-->
      <!--[if !mso]><!-- -->
      <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,700,700i" rel="stylesheet">
      <!--<![endif]-->
    </head>
    <body>
        <div class="es-wrapper-main">
            <!--[if gte mso 9]><v:background xmlns:v="urn:schemas-microsoft-com:vml" fill="t"><v:fill type="tile" color="#ffffff"></v:fill></v:background><![endif]-->
            <table class="es-wrapper" width="100%" cellspacing="0" cellpadding="0">
                <tr>
                    <td valign="top" class="pm0">
                        <table class="es-content" cellspacing="0" cellpadding="0" align="center">
                            <tr></tr>
                            <tr>
                                <td align="center" class="pm0">
                                    <table class="es-header-body" width="600" cellspacing="0" cellpadding="0" bgcolor="#044767" align="center">
                                        <tr>
                                            <td align="left" class="es-hb-1">
                                                <table cellspacing="0" cellpadding="0" width="100%">
                                                    <tr>
                                                        <td class="es-m-p0r pmo" width="530" valign="top" align="center">
                                                            <table width="100%" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td class="es-m-txt-c pmo" align="left">
                                                                        <div class="column" style="width:100%;display:inline-block;vertical-align:top;">
                                                                            <table class="contents" style="border-spacing:0; width:100%">
                                                                                <tr>
                                                                                    <td width="39%" align="right" style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;"><a href=${process.env.WEBSITE_ADD} target="_blank">
                                                                                        <img src=${process.env.LOGO} alt=${process.env.APP_NAME} class="logo-img" width="80" height="80" /></a>
                                                                                    </td>
                                                                                    <td width="61%" align="left" valign="middle" style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;">
                                                                                        <h1 class="app-name">
                                                                                            ${process.env.APP_NAME}
                                                                                        </h1>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                    </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>`,
    footer: `
            <table class="es-footer es.ft-1" cellspacing="0" cellpadding="0" align="center">
                <tr>
                    <td align="center"  class="pm0">
                        <table class="es-footer-body ft-1" width="600" cellspacing="0" cellpadding="0" align="center">
                            <tr>
                                <td align="left" style="Margin:0;padding-top:35px;padding-left:35px;padding-right:35px;padding-bottom:40px;">
                                    <table width="100%" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="530" valign="top" align="center" class="pm0" >
                                                <div class="column" style="width:100%;display:inline-block;vertical-align:top;">
                                                    <table class="contents" style="border-spacing:0; width:100%">
                                                        <tr>
                                                            <td width="39%" align="right" style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;">
                                                            <a href=${process.env.WEBSITE_ADD} target="_blank">
                                                                <img src=${process.env.LOGO} alt=${process.env.APP_NAME} class="logo-img" width="80" height="80" /></a>
                                                            </td>
                                                            <td width="61%" align="left" valign="middle" style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;">
                                                                <p class="address-p">
                                                                    <strong>${process.env.ADD_1}</strong>
                                                                </p>
                                                                <p class="address-p">
                                                                    <strong>${process.env.ADD_2}</strong>
                                                                </p>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <table width="100%" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        </tr>
                                                            <tr>
                                                                <td class="es-m-txt-c" esdev-links-color="#777777" align="left" class="pm0" style="padding-bottom:5px;">
                                                                    <p class="footer-p" style="margin-top: 20px; margin-bottom: 20px">
                                                                        Always push forward | Toujours avancer
                                                                    </p>
                                                                    <p class="footer-p" style="margin-bottom: 20px">
                                                                        If you didn't create an account using this email address, please ignore this email.
                                                                    </p>
                                                                    <p class="footer-p">
                                                                        Si vous n'avez pas créé de compte en utilisant cette adresse e-mail, veuillez ignorer cet e-mail.
                                                                    </p>
                                                                </td>
                                                            </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </body>
    </html>
    `
}
