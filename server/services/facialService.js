const AWS = require("aws-sdk");
require('dotenv').config({ path: 'variables.env' });

// NB: regoknition must be initiated with image for search

AWS.config.update({
    region: process.env.REGION,
    accessKeyId: process.env.ACCESSKEYID,
    secretAccessKey: process.env.SECRETACCESSKEY,
});

const s3 = new AWS.S3({ apiVersion: "2006-03-01" });
const rekognition = new AWS.Rekognition({ apiVersion: "2016-06-27" });

module.exports = class facialService {

    constructor(type = 'national') {
        this.bucket = type === 'user' ? process.env.S3_BUCKET_USER : process.env.S3_BUCKET_NATIONAL;
        this.collectionId = type === 'user' ? process.env.COLLECTION_ID_USER : process.env.COLLECTION_ID_NATIONAL;
    }

    set request(image) {
        this.imageBuffer = new Buffer.from(image, 'base64')
    }

    getKey() {
        return this.key;
    }

    getBuckets() {
        s3.listBuckets((err, data) => {
            if (err) console.log(err, err.stack);
            else console.log(data); // successful response
        });
    }

    getImageUrl(key) {
        return `https://${this.bucket}.s3.eu-west-1.amazonaws.com/${key}`;
    }

    detectFace(image = this.imageBuffer) {
        const buffer = new Buffer.from(image, 'base64')
        const params = {
            Image: {
                Bytes: buffer
            },
            Attributes: [
                "DEFAULT"
            ]
        };
        return new Promise((resolve, reject) => {
            // todo image proximity
            rekognition.detectFaces(params, (err, data) => {
                if (err) {
                    resolve(err);
                } else {
                    const faceDetail = data.FaceDetails;

                    if (faceDetail.length === 0 || faceDetail.length > 1) {
                        resolve({ success: false, error: 'Check that only a single visible face is available' });
                    } else if ( Math.round(faceDetail[0]['Quality']['Brightness']) < 60 ) {
                        resolve({ success: false, error: 'Image quality not bright enough' });
                    } else if (Math.round(faceDetail[0]['Quality']['Sharpness']) < 90) {
                        resolve({ success: false, error: 'Image quality not sharp enough' });
                    } else if (Math.round(faceDetail[0]['Confidence']) < 99) {
                        resolve({ success: false, error: 'Please retake image, Face not displayed well' });
                    } else {
                        resolve({success: true, faceDetail});
                    }
                }
            })
        });
    };


    async searchByImage(image = this.imageBuffer) {
        const params = {
            CollectionId: this.collectionId,
            Image: {
                Bytes: this.imageBuffer
            }
        };
        const correctImage = await this.detectFace(image);
        if (correctImage.success) {
            return new Promise((resolve, reject) => {
                rekognition.searchFacesByImage(params, (err, data) => {
                    if (err) console.log({ err, stack: err.stack });
                    else resolve({success: true, data});
                });
            });
        } else {
            return correctImage
        }
    };

    newFace(userKey) {
        return Promise.all([this.uploadImage(userKey), this.indexImage(userKey)]).then(res => ({ success: true, data: { ...res[0], ...res[1], key: this.key } }));
    }

    uploadImage(userKey) {
        const params = {
            Bucket: this.bucket,
            Key: userKey,
            ACL: "public-read",
            Body: this.imageBuffer,
            ContentEncoding: 'base64',
            ContentType: 'image/jpeg'
        };
        return new Promise((resolve, reject) => {
            s3.upload(params, (err, data) => {
                if (err) return console.log(err);
                resolve({ location: data.Location })
            });
        });
    };

    indexImage(userKey) {
        const params = {
            CollectionId: this.collectionId,
            DetectionAttributes: [],
            ExternalImageId: userKey,
            Image: {
                Bytes: this.imageBuffer
            },
            MaxFaces: 1
        };

        return new Promise((resolve, reject) => {
            rekognition.indexFaces(params, (err, data) => {
                if (err) console.log(err, err.stack);
                else resolve({ indexLocation: data.FaceRecords[0].Face.FaceId });
            });
        });
    };


    listFaces() {
        return new Promise((resolve, reject) => {
            const params = {
                CollectionId: this.collectionId
            };
            rekognition.listFaces(params, (err, data) => {
                if (err) console.log(err, err.stack);
                else resolve(data);
            });
        });
    };

    listAllImages() {
        const params = {
            Bucket: this.bucket
        };

        return new Promise((resolve, reject) => {
            s3.listObjectsV2(params, (err, data) => {
                if (err) console.log(err, err.stack);
                else resolve(data);
            });
        });
    };

    deleteUser(user) {
        return Promise.all([this.deleteImages(user['face']['key']), this.deleteIndex([user['face']['indexLocation']])]).then(res => res)
    }

    deleteImages(Key) {
        const params = {
            Bucket: this.bucket,
            Key
        };
        return new Promise((resolve, reject) => {
            s3.deleteObject(params, (err, data) => {
                if (err) return console.log(err, err.stack);
                resolve(data);
            });
        });
    };

    deleteIndex(key = []) {
        const params = {
            CollectionId: this.collectionId,
            FaceIds: key
        };
        return new Promise((resolve, reject) => {
            rekognition.deleteFaces(params, (err, data) => {
                if (err) return console.log(err, err.stack);
                resolve(data);
            });
        });
    };

}
