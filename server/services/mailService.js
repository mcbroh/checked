"use strict";
const transporter = require('./email/transporter');
const template = require('./email/templates');
const password = require('./email/password');

module.exports = class MailService {

    constructor(email, name, password = null, username = null) {
        this.receiverEmail = process.env.NODE_ENV === 'development' ? 'mcbroh@msn.com' : email;
        this.password = password;
        this.name = name
        this.username = username;
    }

    async passwordResetMail(url) {
        if (process.env.NODE_ENV === 'development') {
            return;
        }
        await transporter.createTransport.verify();
        await transporter.createTransport.sendMail({
            from: process.env.MAIL_NO_REPLY,
            to: this.receiverEmail,
            subject: process.env.APP_NAME + " Password Reset",
            html: template.mainEmailTemplate(this.name, password.resetPasswordTemplate(url))
        });
    }


    async transactionMail(type) {
        if (process.env.NODE_ENV === 'development') {
            return;
        }
        await transporter.createTransport.verify();
        await transporter.createTransport.sendMail({
            from: process.env.MAIL_NO_REPLY,
            to: this.receiverEmail,
            subject: "Transaction notification",
            html: template.mainEmailTemplate(this.name, this.generateNewTransactionMsg(type))
        });
    }

    generateNewTransactionMsg(type) {
        let template = null;
        switch (type) {
            case 'new':
                template = this.getNewTransactionMsg();
                break;
            default:
                template = this.getTransactionStatusChangeMsg();
        };

        return `<tr>
            <td align="left" class="td-sf-01">
                <table width="100%" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="530" valign="top" align="center" class="pm0">
                            <table width="100%" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td align="center" class="pm0 pt15">
                                        <p>${template}</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" class="pm0 td-sf-02">
                                        <p>${this.appAddress()}</p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>`;
    }

    getNewTransactionMsg() {
        /*  return `A transaction has been registered with you. You can check the status of your transaction and report the activities in our mobile client application.` */
        return `Une transaction a été enregistrée avec vous. Vous pouvez vérifier l'état de votre transaction et signaler les activités dans notre application mobile client.`
    }

    getTransactionStatusChangeMsg() {
        /*  return `One or more of your transactions have been updated. Check your application for more details.` */
        return `Un ou plusieurs de vos transactions ont été mis à jour. Vérifiez votre application pour plus de détails.`
    }

    appAddress() {
        //return `client mobile app comming soon`
        return 'Primum mobile app adventum client';
    }

    async newUser(endUser = false) {
        if (process.env.NODE_ENV === 'development') {
            return;
        }
        await transporter.createTransport.verify();
        await transporter.createTransport.sendMail({
            from: process.env.MAIL_EMAIL,
            to: this.receiverEmail,
            subject: "Welcome to " + process.env.APP_NAME,
            html: template.mainEmailTemplate(this.name, this.generateNewUserMessage(endUser), this.generateIntro())
        });
    }

    generateNewUserMessage(endUser) {
        return `
    <tr>
        <td align="left" class="td-sf-01">
            <table width="100%" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="530" valign="top" align="center" class="pm0">
                        <table width="100%" cellspacing="0" cellpadding="0">
                            <tr>
                                <td align="center" class="pm0">
                                    <h2>NEXT STEP</h2>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" class="pm0 pt15">
                                    <p>${endUser ? this.generateEndUserMsg() : this.generateMsg()}</p>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" class="pm0 td-sf-02">
                                    <span class="button es-button-border">
                                        <a href="www.jollo" class="es-button" target="_blank">
                                            ${this.getBtnMsg()}
                                        </a>
                                    </span>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>`;
    }

    generateEndUserMsg() {
        /* Your new Username is: <br><b><u>${this.username}</u></b> <br>
         We will be rolling out an app where by you can check your credit score soon. <br>
         Once this is ready you will be contacted with your username and password. <br><br><br><br> */

        return ` Votre nom d'utilisateur est: <br> <b> <u> ${this.username} </u> </b> <br>
         Une application où vous pourrez vérifier votre pointage de crédit sera bientôt disponible. <br>
         Une fois que cela est prêt, vous serez contacté avec votre nom d'utilisateur et mot de passe
        `
    }

    generateIntro() {
        /* Welcome to ${process.env.APP_NAME} and thanks for signing up! You're one step closer to completion.
        <br><br> */
        return `Bienvenue sur <strong>${process.env.APP_NAME}</strong> et merci de votre inscription! Vous êtes un peu plus près de l'achèvement.`
    }

    generateMsg() {
        /*  Your new Username is: <br><b><u>${this.username}</u></b> <br>
          Your new Password is: <br><b><u>${this.password}</u></b> <br>
          log in with it and you are all set.
          <br><br> */
        if (this.username) {
            return ` Votre nouveau nom d'utilisateur est: <br> <b> <u> ${this.username} </u> </b> <br>
         Votre nouveau mot de passe est le suivant: <br> <b> <u> ${this.password} </u> </b> <br>
         connectez-vous avec elle et vous êtes tous ensemble.
        `
        } else {
            /* Thank you for your application.<br>
            You will be mailed or contacted when your application is completed.<br>
            <b><u>Application id: ${this.password}</u></b>
            <br><br> */
            return ` Merci pour votre candidature. <br>
         Vous serez posté ou contacté une fois votre demande complétée. <br>
         <b> <u> Identifiant de l'application: ${this.password} </u> </b>
        `
        }
    }

    getBtnMsg() {
        return 'Aller sur le site' /* 'Go to site' */
    }

}


