const AWS = require("aws-sdk");
require('dotenv').config({ path: 'variables.env' });

// NB: regoknition must be initiated with image for search

AWS.config.update({
    region: process.env.REGION,
    accessKeyId: process.env.ACCESSKEYID,
    secretAccessKey: process.env.SECRETACCESSKEY,
});

const s3 = new AWS.S3({ apiVersion: "2006-03-01" });

module.exports = class ImageService {
    setPropertyImages(key, image) {

        const imgBuffer = new Buffer.from(image.split('base64,')[1], 'base64')

        return s3.putObject({
            Bucket: process.env.S3_BUCKET_PROPERTY,
            Key: key,
            Body: imgBuffer,
            ACL: "public-read",
            ContentEncoding: 'base64',
            ContentType: 'image/png'
        }).promise()
    }

    async deletePropertyImage(key) {
        return s3.deleteObject({
            Bucket: process.env.S3_BUCKET_PROPERTY,
            Key: key,
        }, function (err, data) {
            if (err) {
                return {success: false, err}
            } else {
                return {success: true, data}
            }
        })
    }
}
