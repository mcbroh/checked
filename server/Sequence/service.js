const Sequence = require("./model");

module.exports = class SequenceService {

    constructor(name) {
        this.query = { name };
	}

    get sequence() {
        return this.getSequence();
    }

    async getSequence() {
        try {
            const sequence = await Sequence.findOne(this.query);
            if (sequence) {
                return this.getLastSeq(sequence.seq);
            } else {
                return this.setSeq();
            }
		} catch (error) {
			return this.errorMessage(error);
		}
    }

    async getLastSeq(seq) {
        const lastSeq = seq + 1;
        const newSeq = { $set: {seq: lastSeq } };
        try {
            await Sequence.updateOne(this.query, newSeq);
            return lastSeq
        } catch (error) {
            return this.errorMessage(error);
        }
    }

    async setSeq() {
        const sequence = new Sequence(this.query);
        try {
            await sequence.save();
            return sequence.seq;
        } catch (error) {
            return this.errorMessage(error);
        }
    }

    errorMessage(error) {
		return { success: false, message: error.message};
	}
}


