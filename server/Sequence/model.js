const mongoose = require('mongoose');
const _Constants = require('./constants');

sequenceSchema = mongoose.Schema({
    name: {
        type: String,
        required: true,
    },
    seq: {
        type: Number,
        required: true,
        default: _Constants.initialSequence,
    },
});


module.exports = mongoose.model('Sequence', sequenceSchema);
