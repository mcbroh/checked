
const passport = require('passport');
const { ExtractJwt } = require('passport-jwt');
const JWTStrategy = require('passport-jwt').Strategy;
const LocalStrategy = require('passport-local').Strategy;

const Company = require('../Company/model');

passport.use(new JWTStrategy({
  jwtFromRequest: ExtractJwt.fromHeader('authorization'),
  secretOrKey: process.env.JWT_SECRET
}, async (payload, done) => {
  try {
    const user = await Company.findById(payload.sub);

    if (!user) return done(null, false);

    done(null, user);

  } catch (error) {
    done(error, false)
  }
}));

passport.use(new LocalStrategy( async (username, password, done) => {
    username = username.toLowerCase();
  try {
    const user = await Company.findOne({ username });

  if (!user) return done(null, false);

  const isCorrectPassword = await user.isValidPassword(password);

  if (!isCorrectPassword) return done(null, false);

  done(null, user);
  } catch (error) {
    done(error, false);
  }
}));

