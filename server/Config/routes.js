const express = require('express');
const router = new express.Router();

const ConfigController = require('./controller');

router.get('/', ConfigController.config);

module.exports = router;
