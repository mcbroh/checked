const ConfigService = require('./service');

module.exports = {
    config: async (req, res) => {
        try {
            const configService = new ConfigService();
            const response = await configService.getConfig();
            return res.status(200).send(response);
        } catch (err) {
            return res.status(200).send({ success: false, message: err });
        }
    },
}

