
const CountriesList = require('./countriesList');
var packageJson = require('../../package.json');

module.exports = class ConfigService {

    async getConfig() {
        return {
            success: true, data: {
                countriesList: CountriesList.CountriesList,
                appVersion: packageJson.version,
                rand: Math.random(),
            }
        }
    }
}
