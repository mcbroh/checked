const CountriesList = [
    {
        name: "Cote D'Ivoire (Ivory Coast)",
        code: "CI",
        region: [
            { name: "Abidjan" },
            { name: "Bas-Sassandra" },
            { name: "Comoe" },
            { name: "Denguele" },
            { name: "Goh-Djiboua" },
            { name: "Lacs" },
            { name: "Lagunes" },
            { name: "Montagnes" },
            { name: "Sassandra-Marahoue" },
            { name: "Savanes" },
            { name: "Vallee du Bandama" },
            { name: "Woroba" },
            { name: "Yamoussoukro Autonomous District" },
            { name: "Zanzan" }
        ]
    },
    {
        name: "Nigeria",
        code: "NG",
        region: [
            { name: "Abia State" },
            { name: "Adamawa State" },
            { name: "Akwa Ibom State" },
            { name: "Anambra State" },
            { name: "Bauchi State" },
            { name: "Bayelsa State" },
            { name: "Benue State" },
            { name: "Borno State" },
            { name: "Cross River State" },
            { name: "Delta State" },
            { name: "Ebonyi State" },
            { name: "Edo" },
            { name: "Ekiti State" },
            { name: "Enugu State" },
            { name: "Federal Capital Territory" },
            { name: "Gombe State" },
            { name: "Imo State" },
            { name: "Jigawa State" },
            { name: "Kaduna State" },
            { name: "Kano State" },
            { name: "Katsina State" },
            { name: "Kebbi State" },
            { name: "Kogi State" },
            { name: "Kwara State" },
            { name: "Lagos State" },
            { name: "Nasarawa State" },
            { name: "Niger State" },
            { name: "Ogun State" },
            { name: "Ondo State" },
            { name: "Osun State" },
            { name: "Oyo State" },
            { name: "Plateau State" },
            { name: "Rivers State" },
            { name: "Sokoto State" },
            { name: "Taraba State" },
            { name: "Yobe State" },
            { name: "Zamfara State" }
        ]
    },
    {
        name: "Senegal",
        code: "SN",
        region: [
            { name: "Dakar" },
            { name: "Diourbel" },
            { name: "Fatick" },
            { name: "Kaolack" },
            { name: "Kolda" },
            { name: "Louga" },
            { name: "Matam" },
            { name: "Region de Kaffrine" },
            { name: "Region de Kedougou" },
            { name: "Region de Sedhiou" },
            { name: "Region de Thies" },
            { name: "Saint-Louis" },
            { name: "Tambacounda" },
            { name: "Ziguinchor" }
        ]
    }
];

module.exports = {
    CountriesList,
    Currency: {
        CI: 'CFA',
        NG: 'NGN',
        SN: 'CFA'
    }
};
