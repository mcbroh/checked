const Joi = require("@hapi/joi");

const levels = {
	SUPER_ADMIN: 'superAdmin',
	SUB_SUPER_ADMIN: 'subSuperAdmin',
	ADMIN: 'admin',
	SUB_ADMIN: 'subAdmin',
	ODINARY_USER: 'ordinaryUser'
}

module.exports = {
	levels: levels,
	setPrevilege: (privilege) => {
		if (!privilege) {
			throw new Error('privilege is not defined')
		}
		switch (privilege) {
			case levels.SUPER_ADMIN:
				return Joi.string().valid(
					levels.SUPER_ADMIN,
					levels.SUB_SUPER_ADMIN,
				).required()

			case levels.ADMIN:
				return Joi.string().valid(
					levels.SUB_ADMIN
				).required()
		}
	},
}
