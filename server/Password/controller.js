const jwt = require('jsonwebtoken');
const Company = require("../Company/model");
const password = require('../services/email/password');
const MailService = require('../services/mailService');
const passwordGen = require('../helpers/passwordGen');


module.exports = {

    sendPasswordResetEmail: async (req, res) => {
        const { email, type } = req.body
        let user
        try {
            user = await Company.findOne({ email });
        } catch (err) {
            res.status(404).send({ success: false, error: err })
            return;
        }

        if (!user) {
            res.send({ success: false, message: 'User does not esist' });
            return;
        }

        const name = user.name || user.firstName + ' ' + user.lastName;
        const token = getPasswordHash(user)

        const url = password.getPasswordResetURL(user, token);

        await updateUser(type, email, token).then(() => {
            const mailer = new MailService(user.email, name);
            mailer.passwordResetMail(url).then(response => {
                res.send({ success: true })
            }).catch(err => res.send({ success: false, message: err.message }));
        }).catch(err => res.send({ success: false, message: err.message }));

    },

    receiveNewPassword: async (req, res) => {
        const { userId, token, password } = req.body
        const secret = password + "_" + process.env.JWT_SECRET;
        const payload = jwt.decode(token, secret);
        let user;

        try {
                user = await Company.findOne({ _id: userId });
                if (payload._id === user.id && user.passwordResetToken === token) {
                    const hashedPassword = await passwordGen.genHashedPassword(password);
                    await Company.findOneAndUpdate({ _id: userId }, { password: hashedPassword, passwordResetToken: 'used' })
                        .then(() => {
                            res.send({ success: true });
                            return;
                        })
                        .catch(err => {
                            res.status(500).json(err);
                            return;
                        })
                } else {
                    res.status(404).send({status: false, message: "Invalid user"})
                    return;
                }
        } catch (err) {
            res.status(404).send({status: false, message: err})
        }
    }
}

function updateUser(dbType, email, value) {
    return Company.findOneAndUpdate({ email }, { passwordResetToken: value });
}

function getPasswordHash({ password, _id }) {
    const secret = password + "_" + process.env.JWT_SECRET;
    const token = jwt.sign({ _id }, secret, {
        expiresIn: 3600 // 1 hour
    })
    return token
}
