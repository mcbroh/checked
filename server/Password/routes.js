const express = require('express');
const router = new express.Router();

const PasswordController = require('./controller');

router.post('/reset', PasswordController.sendPasswordResetEmail)
router.post('/new', PasswordController.receiveNewPassword)


module.exports = router;
