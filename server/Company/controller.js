const Company = require("./model");
const Joi = require("@hapi/joi");
const CompanyDTO = require('./DTO');
const userLevels = require('../helpers/userLevels');
const CompanyService = require('./service');


module.exports = {

    newCompany: async (req, res) => {
        const schema = {
            name: Joi.required(),
            city: Joi.required(),
            phone: Joi.required(),
            street: Joi.required(),
            streetNo: Joi.required(),
            companyId: Joi.required(),
            companyName: Joi.required(),
            country: Joi.required(),
            email: Joi.string().email().required(),
        };

        try {
            await Joi.validate(req.body, schema);
        } catch (err) {
            return res.status(200).send({ success: false, message: err.details[0].message });
        }

        const CompanyDto = new CompanyDTO(req.body)

        try {
            const companyService = new CompanyService();
            const user = await companyService.createCompany(CompanyDto);
            res.send(user)
        } catch (e) {
            console.log('error: ', e.message);

            res.status(500).send(e.message);
        }
    },

    postUser: async (req, res) => {

        const { privilege } = req.user;

        if (!privilege || privilege === userLevels.levels.SUB_ADMIN || privilege === userLevels.levels.SUB_SUPER_ADMIN) {
            return res.status(401).send({ message: 'You do not have the right privilege' });
        }

        const schema = {
            name: Joi.required(),
            username: Joi.string().min(3).max(15),
            email: Joi.string().email().required(),
        };

        try {
            await Joi.validate(req.body, schema);
        } catch (err) {
            return res.status(200).send({ success: false, message: err.details[0].message });
        }

        try {
            const companyDto = new CompanyDTO();
            const newUser = await companyDto.setCompanyEmployee(req.body, req.user);

            const companyService = new CompanyService();
            const user = await companyService.createUser(newUser);

            res.send(user)
        } catch (e) {
            console.log('error: ', e.message);

            res.send({ succes: false, message: e.message });
        }
    },

    deleteUser: async (req, res) => {
        if (req.body.user) {
            await removeUser(req, res)
        } else {
            await closeMyAccount(req, res);
        }
    },



    getToken: async (req, res) => {
        try {
            const companyService = new CompanyService();
            const user = companyService.logMeIn(req.user);
            res.status(200).send(user)
        } catch (e) {
            console.log('error: ', e.message);

            res.status(500).send(e.message);
        }
    },

    getUsers: async (req, res) => {
        try {
            const companyService = new CompanyService();
            const users = await companyService.getUsersByLevel(req.user);

            res.send(users)
        } catch (e) {
            console.log('error: ', e.message);

            res.status(500).send();
        }
    },

    getSingleUser: async (req, res) => {
        const _id = req.user.id;
        try {
            const user = await Company.findById(_id);
            res.send(user);
        } catch (e) {
            console.log('error: ', e.message);

            res.status(500).send(e.message);
        }
    },

    passwordUpdate: async (req, res) => {
        const schema = {
            oldPassword: Joi.required(),
            password: Joi.string().min(6).required(),
        };

        try {
            await Joi.validate(req.body, schema);
        } catch (err) {
            return res.status(200).send({ success: false, message: err.details[0].message });
        }

        try {
            const companyService = new CompanyService();
            const users = await companyService.updatePassword(req.body, req.user);
            res.send(users)
        } catch (e) {
            console.log('error: ', e.message);

            res.status(500).send(e.message);
        }
    },

};

async function closeMyAccount(req, res) {
    if (req.user.privilege === userLevels.levels.SUPER_ADMIN) {
        return res.status(400).send({ success: false, message: 'You can not delete your self.' });
    }
    await doDeleteUser(req.user.id, req, res);
};

async function removeUser(req, res) {
    const schema = {
        user: Joi.required()
    };

    try {
        await Joi.validate(req.body, schema);
    } catch (err) {
        return res.status(200).send({ success: false, message: err.details[0].message });
    }

    const _userId = req.body.user;

    if (
        _userId === req.user._id
        &&
        req.user.privilege === userLevels.levels.SUPER_ADMIN
    ) {
        return res.status(400).send({ success: false, message: 'You can not delete your self.' });
    }

    await doDeleteUser(_userId, req, res);
};

async function doDeleteUser(_userId, req, res) {
    try {
        const companyService = new CompanyService();
        const user = await companyService.deleteUserByLevel(_userId, req.user);
        res.send(user);
    }
    catch (e) {
        console.log('error: ', e.message);
        res.status(500).send({ success: false, message: e.message });
    }
}

