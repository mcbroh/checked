const Company = require("./model");
const Property = require("../Property/model");
const PropertyService = require('../Property/service');
const JWT = require("jsonwebtoken");
const bcrypt = require('bcryptjs');
const userLevels = require('../helpers/userLevels');
const SequenceService = require('../Sequence/service');
const MailService = require('../services/mailService');
const passwordGen = require('../helpers/passwordGen');

const signToken = user => {
    return JWT.sign(
        {
            iss: "ChekD",
            sub: user._id
        },
        process.env.JWT_SECRET
    );
}

module.exports = class CompanyService {

    errorMessage(error) {
        return { success: false, message: error.message };
    }

    userData(user) {
        return { success: true, ...user }
    }

    logMeIn(loggedInUser) {
        const user = JSON.parse(JSON.stringify(loggedInUser));
        delete user["password"];
        return this.userData({ token: signToken(user), user: user });
    }

    getUsersByLevel(user) {
        switch (user.privilege) {
            case userLevels.levels.SUPER_ADMIN:
            case userLevels.levels.SUB_SUPER_ADMIN:
                return this.getAllUsers()
            default:
                // case userLevels.levels.ADMIN:
                // case userLevels.levels.SUB_ADMIN:
                return this.getAllUsersWithinMyCompany(user.trueId)
        }
    }

    async getAllUsers() {
        try {
            const users = await Company.find({}).select(['-password', '-passwordResetToken']);
            const filteredOutCompanyEmployees = users.filter(user => user.privilege !== userLevels.levels.SUB_ADMIN);
            return this.userData({ users: filteredOutCompanyEmployees });
        } catch (e) {
            return this.errorMessage(e);
        }
    }


    async getAllUsersWithinMyCompany(trueId) {
        try {
            const users = await Company.find({ trueId }).select(['-password', '-passwordResetToken']);

            return { success: users !== null, users };
        } catch (e) {
            return this.errorMessage(e);
        }
    }

    async createUser(dataToMutate) {
        const hashedPassword = await passwordGen.genHashedPassword(dataToMutate.password);
        const user = new Company(dataToMutate);
        user['password'] = hashedPassword;
        try {
            await user.save();

            const { email, name, username, password } = dataToMutate;
            const mailer = new MailService(email, name, password, username);
            mailer.newUser();

            return this.userData({ email, name, username });
        }
        catch (e) {
            return this.errorMessage(e);
        }
    }

    async createCompany(user) {
        const hashedPassword = await passwordGen.genHashedPassword(user.password);
        const sequenceService = new SequenceService('company');
        const username = await sequenceService.sequence;

        user = user.setCompany;
        user['trueId'] = username;
        user['username'] = username;
        user['privilege'] = userLevels.levels.ADMIN;

        const company = new Company(user);
        company.password = hashedPassword;
        try {
            await company.save();

            const { email, name, username, id } = company;
            const mailer = new MailService(email, name, user.password, username);
            mailer.newUser();

            return this.userData({ email, name, username, id });
        }
        catch (e) {
            return this.errorMessage(e);
        }
    }

    deleteUserByLevel(userId, arg) {
        switch (arg.privilege) {
            case userLevels.levels.SUPER_ADMIN || userLevels.levels.SUB_SUPER_ADMIN:
                return this.deleteUser(userId)
            default:
                // case userLevels.levels.ADMIN:
                return this.deleteUserWithinMyCompany(userId, arg.trueId)
        }
    }

    async deleteUser(userToDeleteId) {
        try {

            let users = null;
            const company = await Company.findById(userToDeleteId);

            if (!company.username.includes("@") && company.trueId) {
                users = await Company.find({ trueId: company.trueId });
            }

            if (users) {
                for (const user of users) await user.remove();
            }

            await company.remove();

            return this.userData({ company });
        } catch (error) {
            return this.errorMessage(error);
        }

    }

    async deleteUserWithinMyCompany(userToDeleteId, signedInUserTrueId) {
        try {
            // delete all properties created by me
            // get all properties
            // deleteProperty
            const listOfAdverts = await Property.find({ trueId: signedInUserTrueId }).distinct('_id');
            for (const advertId of listOfAdverts) {
                const property = new PropertyService();
                await property.deleteProperty(signedInUserTrueId, advertId);
            }

            const deletedUsers = await Company.deleteMany({ trueId: signedInUserTrueId });

            return this.userData({ count: deletedUsers.deletedCount });
        } catch (error) {
            return this.errorMessage(error);
        }
    }

    async updatePassword(passwordData, signedInUser) {
        const res = await bcrypt.compare(passwordData.oldPassword, signedInUser.password);

        if (!res) return this.errorMessage({ message: 'Wrong password' });

        try {
            const password = await passwordGen.genHashedPassword(passwordData.password);
            await Company.findByIdAndUpdate(signedInUser._id, { password });
            return this.userData({ message: 'Success' })
        } catch (error) {
            return this.errorMessage(error);
        }
    }

}
