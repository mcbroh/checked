const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const moment = require('moment');
const uniqueValidator = require('mongoose-unique-validator');

const currentDateAndTime = moment().format('YYYY MM DD, HH:mm');

const commonAttr = {
    type: String,
    trim: true
};

const requiredAttr = {
    ...commonAttr,
    required: true,
};


const companySchema = mongoose.Schema({
    name: commonAttr,
    username: {
        ...commonAttr,
        unique: true,
    },
    trueId: commonAttr,
    email: {
        ...requiredAttr,
        unique: true,
        lowercase: true,
    },
    password: requiredAttr,
    temporaryPassword: commonAttr,
    passwordResetToken: commonAttr,
    privilege: {
        type: String,
        require: true
    },
    company: {
        name: { ...requiredAttr },
        city: { ...requiredAttr },
        phone: { ...requiredAttr },
        street: { ...requiredAttr },
        streetNo: { ...requiredAttr },
        country: { ...requiredAttr },
        companyId: {
            ...requiredAttr,
            unique: true,
        },
        organization: { ...commonAttr },
        organizationPhone: { ...commonAttr }
    },
});

companySchema.plugin(uniqueValidator);

companySchema.methods.isValidPassword = async function (newPassword) {
    try {
        return await bcrypt.compare(newPassword, this.password);
    } catch (error) {
        throw new Error(error);
    }
}

module.exports = mongoose.model('Company', companySchema);
