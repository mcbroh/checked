const express = require('express');
const router = new express.Router();
const passport = require('passport');
const passportConfig = require('../Config/userPassport');
const CompanyController = require('./controller');
const passportJWT = passport.authenticate('jwt', { session: false });
const passportSignIn = passport.authenticate('local', { session: false });

router.get('/myUsers', passportJWT, CompanyController.getUsers);
router.get('/:id', passportJWT, CompanyController.getSingleUser);
router.get('/', passportSignIn, CompanyController.getToken);

router.post('/user', passportJWT, CompanyController.postUser);
router.post('/', CompanyController.newCompany);
router.patch('/', passportJWT, CompanyController.passwordUpdate);
router.delete('/', passportJWT, CompanyController.deleteUser);


module.exports = router;
