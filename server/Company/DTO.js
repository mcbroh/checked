const userLevels = require('../helpers/userLevels');

const randomString = (num = 3) => {
    const passwordMemes = ['benin', 'nigeria', 'ivoryCoast', 'senegal', 'togo', 'gambia', 'benin', 'mali', 'liberia', 'niger']
    const temporaryPassword = passwordMemes[Math.floor(Math.random() * 10)] + Math.random().toString(36).substring(2, num) + Math.random().toString(36).substring(2, num);
    return process.env.NODE_ENV === 'development' ? 'password' : temporaryPassword;
};

module.exports = class CompanyDTO {

    constructor(company = null) {
        if (company) {
            this.name = company.name;
            this.city = company.city;
            this.email = company.email.toLowerCase();
            this.phone = company.phone;
            this.street = company.street;
            this.username = company.username || Date.now(),
            this.streetNo = company.streetNo;
            this.companyId = company.companyId;
            this.companyName = company.companyName;
            this.country = company.country;
            this.password = company.password || randomString();
            this.organization = company.organization || '',
            this.organizationPhone = company.organizationPhone || ''
        };
    }

    get userPassword() {
        return this.password || randomString();
    }

    get setCompany() {
        return {
            name: this.name,
            email: this.email,
            username: this.username,
            password: this.password,
            company: {
                city: this.city,
                phone: this.phone,
                street: this.street,
                name: this.companyName,
                streetNo: this.streetNo,
                companyId: this.companyId,
                country: this.country,
                organization: this.organization,
                organizationPhone: this.organizationPhone
            }
        };
    }

    setCompanyEmployee(employee, signedInCompany) {
        const username = `${employee.username.toLowerCase()}@${signedInCompany.username}`;
        const company = {...signedInCompany.company, companyId: signedInCompany.company.companyId + '-' + username}
        return {
            company,
            username,
            name: employee.name,
            email: employee.email,
            password: randomString(),
            trueId: signedInCompany.username,
            privilege: this.setPrivileged(signedInCompany),
        }
    }

    setPrivileged(user) {
        if (user.privilege === userLevels.levels.SUPER_ADMIN)
            return userLevels.levels.SUB_SUPER_ADMIN;
        if (user.privilege === userLevels.levels.ADMIN)
            return userLevels.levels.SUB_ADMIN;
    }
}
