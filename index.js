const express = require("express");
const path = require('path');
const cors = require('cors');
const fileUpload = require("express-fileupload");
const bodyParser = require('body-parser');

require('dotenv').config({ path: 'variables.env' });
require('./server/Config/mongoose');

const app = express();
app.use(cors());

// Middleware
app.use(bodyParser.urlencoded({ limit: '10mb', extended: true }));
app.use(bodyParser.json({ limit: '50mb', extended: true }));
app.use(fileUpload());
app.use(express.static(__dirname + '/build/'));

// Routes
app.use('/api/companies', require('./server/Company/routes'));
app.use('/api/password', require('./server/Password/routes'));
app.use('/api/property', require('./server/Property/routes'));
app.use('/api/config', require('./server/Config/routes'));

process.on('unhandledRejection', err => {
    console.log(err);
});

app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname + '/build/', 'index.html'))
});

const PORT = process.env.PORT;
app.listen(PORT, () => { console.log(`App listening on port ${PORT}`) });
